#####################################################################
# This module summarises statistics for the RESIDENT and TOURIST    #
# surveys.                                                          #
#####################################################################

module Summariser

  # Example:
  # Summariser.resident_total(3, "2018-05-01", "2018-05-09")
  def self.resident_total(city_id, start_date, end_date)
    # Get users in {city_id} after {start_date}
    users = User.where("user_type = 'resident' and city_id = ? and created_at >= ? and created_at <= ?", 
      city_id, start_date, end_date)
    registered = users.count

    # Get users who completed the survey
    completed_users = users.select { |user| user.resident_survey_completed? }
    completed = completed_users.length

    # Age Group #
    age_groups = {
      "18-30" => completed_users.select { |user| user.age_group_18_30? }.length,
      "31-45" => completed_users.select { |user| user.age_group_31_45? }.length,
      "46-60" => completed_users.select { |user| user.age_group_46_60? }.length,
      "61+" => completed_users.select { |user| user.age_group_61? }.length
    }

    # Gender #
    genders = {
      "male" => completed_users.select { |user| user.gender_male? }.length,
      "female" => completed_users.select { |user| user.gender_female? }.length
    }

    # Driving Licence #
    driving_licences = {
      "car" => completed_users.select { |user| user.driving_licence_car? }.length,
      "moto" => completed_users.select { |user| user.driving_licence_moto? }.length,
      "none" => completed_users.select { |user| user.driving_licence_none? }.length
    }

    # Car Sharing #
    car_sharing_groups = {
      "yes" => completed_users.select { |user| user.car_sharing_yes? }.length,
      "no" => completed_users.select { |user| user.car_sharing_no? }.length
    }

    # Education Level #
    education_levels = {
      "less_than_high_school" => completed_users.select { |user| user.pre_survey_socio_economic_characteristic.education == "1" }.length,
      "high_school" => completed_users.select { |user| user.pre_survey_socio_economic_characteristic.education == "3" }.length,
      "bachelors" => completed_users.select { |user| user.pre_survey_socio_economic_characteristic.education == "4" }.length,
      "masters" => completed_users.select { |user| user.pre_survey_socio_economic_characteristic.education == "5" }.length,
      "doctoral" => completed_users.select { |user| user.pre_survey_socio_economic_characteristic.education == "6" }.length,
      "other" => completed_users.select { |user| user.pre_survey_socio_economic_characteristic.education == "7" }.length
    }

    {
      "registered" => registered,
      "completed" => completed, 
      "age_groups" => age_groups,
      "genders" => genders,
      "driving_licences" => driving_licences,
      "car_sharing_groups" => car_sharing_groups,
      "education_levels" => education_levels
    }
  end

  # Example:
  # Summariser.tourist_total(3, "2018-05-01", "2018-05-09")
  def self.tourist_total(city_id, start_date, end_date)
    # Get users in {city_id} after {start_date}
    users = User.where("user_type = 'tourist' and city_id = ? and created_at >= ? and created_at <= ?", 
      city_id, start_date, end_date)
    registered = users.count

    # Get users who completed the survey
    completed = users.select { |user| user.tourist_survey_completed? }.length

    {
      "registered" => registered,
      "completed" => completed
    }
  end

  # Example:
  # Summariser.analysis(3, "2018-05-01", "2018-05-31")
  def self.analysis(city_id, start_date, end_date)
    # Get users in {city_id} after {start_date}
    users = User.where("user_type = 'tourist' and city_id = ? and created_at >= ? and created_at <= ?", 
      city_id, start_date, end_date)
    registered = users.count

    # Get users who completed the survey
    completed = users.select { |user| user.tourist_survey_completed? }

    duration_72_hr = 0
    duration_week = 0

    completed.each do |user|
      duration = user.pre_survey.ttravelduration.to_i
      # 1
      if duration == 1
      # 2-4
      elsif duration <= 4
        duration_72_hr += 1
      # >4
      else
        duration_week += 1
      end
    end

    {
      "72-hour" => duration_72_hr,
      "week" => duration_week
    }
  end

  def self.tab_2_data
    [{
      "id": 1,
      "email": "smcguckin0@umich.edu"
    }, {
      "id": 2,
      "email": "gasquith1@nifty.com"
    }, {
      "id": 3,
      "email": "alocarno2@google.ru"
    }, {
      "id": 4,
      "email": "olahrs3@miibeian.gov.cn"
    }, {
      "id": 5,
      "email": "elonghi4@un.org"
    }, {
      "id": 6,
      "email": "mperryn5@t-online.de"
    }, {
      "id": 7,
      "email": "bstitson6@addtoany.com"
    }, {
      "id": 8,
      "email": "sesposita7@wix.com"
    }, {
      "id": 9,
      "email": "vhaswell8@cbc.ca"
    }, {
      "id": 10,
      "email": "imenpes9@nationalgeographic.com"
    }, {
      "id": 11,
      "email": "sbattricka@si.edu"
    }, {
      "id": 12,
      "email": "hcaigerb@indiatimes.com"
    }, {
      "id": 13,
      "email": "bflobertc@weibo.com"
    }, {
      "id": 14,
      "email": "ncristofvaod@mediafire.com"
    }, {
      "id": 15,
      "email": "dstreighte@sun.com"
    }, {
      "id": 16,
      "email": "aschermickf@ameblo.jp"
    }, {
      "id": 17,
      "email": "lwhitwhamg@icq.com"
    }, {
      "id": 18,
      "email": "estorkesh@i2i.jp"
    }, {
      "id": 19,
      "email": "talveni@ebay.com"
    }, {
      "id": 20,
      "email": "cbatecokj@cisco.com"
    }, {
      "id": 21,
      "email": "arangerk@exblog.jp"
    }, {
      "id": 22,
      "email": "ppapacciol@4shared.com"
    }, {
      "id": 23,
      "email": "umacuchadairm@washingtonpost.com"
    }, {
      "id": 24,
      "email": "bspauntonn@reference.com"
    }, {
      "id": 25,
      "email": "bdozdillo@ycombinator.com"
    }, {
      "id": 26,
      "email": "kpoveyp@house.gov"
    }, {
      "id": 27,
      "email": "bmccaughenq@nature.com"
    }, {
      "id": 28,
      "email": "rmapstoner@state.gov"
    }, {
      "id": 29,
      "email": "bjigginss@china.com.cn"
    }, {
      "id": 30,
      "email": "smccarlet@wired.com"
    }, {
      "id": 31,
      "email": "knavarrou@adobe.com"
    }, {
      "id": 32,
      "email": "ljenikv@salon.com"
    }, {
      "id": 33,
      "email": "pgainew@gnu.org"
    }, {
      "id": 34,
      "email": "hdanterx@yale.edu"
    }, {
      "id": 35,
      "email": "bronayney@topsy.com"
    }, {
      "id": 36,
      "email": "jglantzz@goo.ne.jp"
    }, {
      "id": 37,
      "email": "dleyton10@google.pl"
    }, {
      "id": 38,
      "email": "hoaker11@hostgator.com"
    }, {
      "id": 39,
      "email": "tdowse12@so-net.ne.jp"
    }, {
      "id": 40,
      "email": "gcurrom13@vk.com"
    }, {
      "id": 41,
      "email": "ecaselick14@yahoo.co.jp"
    }, {
      "id": 42,
      "email": "bshimuk15@qq.com"
    }, {
      "id": 43,
      "email": "cfontin16@hubpages.com"
    }, {
      "id": 44,
      "email": "astewartson17@google.com.au"
    }, {
      "id": 45,
      "email": "kvaadeland18@topsy.com"
    }, {
      "id": 46,
      "email": "dsimanenko19@google.com"
    }, {
      "id": 47,
      "email": "jliff1a@fotki.com"
    }, {
      "id": 48,
      "email": "apoveleye1b@engadget.com"
    }, {
      "id": 49,
      "email": "emoscon1c@wikispaces.com"
    }, {
      "id": 50,
      "email": "hlambillion1d@indiegogo.com"
    }]
  end

end