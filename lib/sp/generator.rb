require 'jsonpath'

#####################################################################
# This module generates SP experiments to identify user preferences #
# DESIGN 1:                                                         #
# =========                                                         #
# - Generates 3 kinds of plans - Basic, Urban, Extra                #
# > Generator.design_plans                                          #
#####################################################################

module Generator

  def self.flexible(user)
    # Load attributes (based on city)
    city = user.city.name.downcase
    attributes = JSON.parse(IO.read("lib/sp/attributes_resident_#{city}.json"))

    # PLANS TEMPLATE
    plans = attributes["flexible"]
  end

  def self.design_plans(user_type, user)
    case user_type
    when "resident"
      design_plans_for_resident(user)
    when "tourist"
      design_plans_for_tourist(user.pre_survey)
    end
  end

  # Designs 3 types of plans - PAYG, weekly and monthly.
  # New design.
  # Picks a block from the list of available plans.
  # Example: Generator.generate_company_1("manchester", 1)
  def self.generate_company_1(city, pliccarfull)
    blocks = *(1..23)

    # Load attributes and value_map
    json = JSON.parse(IO.read("lib/sp/resident_company_1_#{city}.json"))
    attributes = json["attributes"]
    value_map = json["value_map"]

    # Pick block, and set plans for pages 1/2/3 based on plans
    block = blocks.sample

    plans = {
      "1" => {},
      "2" => {},
      "3" => {}
    }

    ["1", "2", "3"].each do |page|
      block_row = JsonPath.on(attributes, "$..[?(@['block'] == #{block} && @['page'] == #{page})]").first

      # PAYG
      plans[page]["payg"] = company_1_plan(block_row, value_map["payg"], "alt1", pliccarfull)

      # # Weekly
      plans[page]["weekly"] = company_1_plan(block_row, value_map["weekly"], "alt2", pliccarfull)
      
      # # Monthly
      plans[page]["monthly"] = company_1_plan(block_row, value_map["monthly"], "alt3", pliccarfull)
    end

    plans
  end

  def self.company_1_plan(block_row, block_value_map, prefix, pliccarfull)
    modes_array = ["public_transport", "bike_sharing", "taxi", 
      (pliccarfull == 1) ? "car_sharing" : nil].compact
    elements_array = ["plan_fee", "contract_level"]

    plan = {
      "modes" => {},
      "elements" => {}
    }

    # modes
    modes_array.each do |mode|
      value = block_row["#{prefix}.#{mode}"]
      mode_hash = JsonPath.on(block_value_map, "$..#{mode}[?(@.value == #{value})]").first

      symbol = mode_hash["symbol"]
      price = mode_hash["price"]

      plan["modes"][mode] = {
        "value" => value,
        "symbol" => symbol,
        "price" => price
      }
    end

    # other elements
    elements_array.each do |element|
      value = block_row["#{prefix}.#{element}"]
      # Rails.logger.debug "SP DEBUG: #{prefix}.#{element} = #{value}"
      element_hash = JsonPath.on(block_value_map, "$..#{element}[?(@.value == #{value})]").first

      symbol = element_hash["symbol"]
      multiplier = element_hash["multiplier"]

      if symbol == "multiplier"
        plan_price = multiplier * plan["modes"]["public_transport"]["price"]
        symbol = plan_price
      end

      plan["elements"][element] = {
        "value" => value,
        "symbol" => symbol,
        "multiplier" => multiplier
      }
    end

    plan
  end

  # Generates fixed plans for design. # TOURIST
  # This generates plans for all the 3 pages so that price-conditions can be checked and validated.
  # Example: Generator.design_plans_for_tourist(PreSurvey.find(33))
  # Example: Generator.design_plans_for_tourist(PreSurvey.find(32))
  def self.design_plans_for_tourist(pre_survey)
    city = pre_survey.user.city.name.downcase

    # Pick design based on duration, travel-mode and license_status
    # ttravelduration, ttravelmode, tliccarfull
    travel_mode = pre_survey.ttravelmode.to_i
    duration = pre_survey.ttravelduration.to_i
    licence_status = (pre_survey.tliccarfull == 1) ? "licensed" : "non_licensed"

    # Rails.logger.debug "SP DEBUG: m : d : l -> #{travel_mode} : #{duration} : #{licence_status}"

    # 1 -> 24h
    if duration == 1
      design = "design_1"
    # 2-4 -> 72h
    elsif duration <= 4
      design = "design_2"
    # >4 -> 1w
    else
      design = "design_3"
    end

    # Pick appropriate file+json
    if travel_mode == 1 # car
      json = JSON.parse(IO.read("lib/sp/tourist_#{design}_car_#{city}.json"))
      attributes = json["attributes"]
    elsif travel_mode == 4 # plane
      json = JSON.parse(IO.read("lib/sp/tourist_#{design}_plane_#{city}.json"))
      attributes = json["attributes"][licence_status]
    else # other
      json = JSON.parse(IO.read("lib/sp/tourist_#{design}_other_#{city}.json"))
      attributes = json["attributes"][licence_status]
    end

    # Load value_map
    value_map = JSON.parse(IO.read("lib/sp/tourist_value_map_#{city}.json"))

    # Select block and json corresponding to that block
    blocks = *(1..70)
    block = blocks.sample

    plans = {
      "1" => [],
      "2" => [],
      "3" => []
    }

    ["1", "2", "3"].each do |page|
      block_row = JsonPath.on(attributes, "$..[?(@['block'] == #{block} && @['page'] == #{page})]").first

      # Basic
      basic_plan = tourist_plan(block_row, value_map[design]["basic"], "alt1")
      basic_plan["type"] = "basic"
      basic_plan["frequency_label"] = value_map[design]["frequency_label"]
      plans[page] << basic_plan

      # Easy
      easy_plan = tourist_plan(block_row, value_map[design]["easy"], "alt2")
      easy_plan["type"] = "easy"
      easy_plan["frequency_label"] = value_map[design]["frequency_label"]
      plans[page] << easy_plan
      
      # Roamer
      roamer_plan = tourist_plan(block_row, value_map[design]["roamer"], "alt3")
      roamer_plan["type"] = "roamer"
      roamer_plan["frequency_label"] = value_map[design]["frequency_label"]
      plans[page] << roamer_plan
    end

    plans
  end

  # Designs 3 types of plans - Basic, Urban, Extra.
  # New design.
  # Picks a block from the list of available plans.
  # Example: Generator.generate_company_2_non_contextual("manchester", 1, 0, 1)
  def self.generate_company_2_non_contextual(city, pliccarfull, ppassfree, ppassdiscount)
    # Pick block
    blocks = *(1..100)
    block = blocks.sample
    Rails.logger.debug "SP DEBUG: generate_company_2_non_contextual: block -> #{block}"

    # Pick licensed/non_licensed attributes
    json = JSON.parse(IO.read("lib/sp/resident_company_2_#{city}.json"))
    licence_status = (pliccarfull == 1) ? "licensed" : "non_licensed"
    attributes = json["attributes"]["non_contextual"][licence_status]
    value_map = json["value_map"]
    frequency_label = value_map["frequency_label"]

    # Set plans for pages 1/2 based on attributes + block
    plans = {
      "1" => [],
      "2" => []
    }

    ["1", "2"].each do |page|
      block_row = JsonPath.on(attributes, "$..[?(@['block'] == #{block} && @['page'] == #{page})]").first

      # Basic
      plans[page] << company_2_plan(block_row, value_map["basic"], "basic", "alt1", 
        pliccarfull, ppassfree, ppassdiscount, frequency_label)

      # Urban
      plans[page] << company_2_plan(block_row, value_map["urban"], "urban", "alt2", 
        pliccarfull, ppassfree, ppassdiscount, frequency_label)
      
      # Extra
      plans[page] << company_2_plan(block_row, value_map["extra"], "extra", "alt3", 
        pliccarfull, ppassfree, ppassdiscount, frequency_label)
    end

    plans
  end

  # Designs 3 types of plans - Basic, Urban, Extra.
  # Also, pick carrots/sticks.
  # New design.
  # Picks a block from the list of available plans.
  # Example: Generator.generate_company_2_contextual("manchester", 1, 0, 1)
  def self.generate_company_2_contextual(city, pliccarfull, ppassfree, ppassdiscount)
    # Pick block
    blocks = *(1..100)
    block = blocks.sample
    Rails.logger.debug "SP DEBUG: generate_company_2_contextual: block -> #{block}"

    # Pick licensed/non_licensed attributes
    json = JSON.parse(IO.read("lib/sp/resident_company_2_#{city}.json"))
    licence_status = (pliccarfull == 1) ? "licensed" : "non_licensed"
    attributes = json["attributes"]["contextual"][licence_status]
    value_map = json["value_map"]
    frequency_label = value_map["frequency_label"]

    # Set plans for pages 1/2 based on attributes + block
    plans = {
      "plans" => {
        "1" => [],
        "2" => [],
        "3" => []
      },

      "contexts" => {
        "1" => {},
        "2" => {},
        "3" => {}
      }
    }

    ["1", "2", "3"].each do |page|
      block_row = JsonPath.on(attributes, "$..[?(@['block'] == #{block} && @['page'] == #{page})]").first

      # Plans
      # Basic
      plans["plans"][page] << company_2_plan(block_row, value_map["basic"], "basic", "alt1", 
        pliccarfull, ppassfree, ppassdiscount, frequency_label)

      # Urban
      plans["plans"][page] << company_2_plan(block_row, value_map["urban"], "urban", "alt2", 
        pliccarfull, ppassfree, ppassdiscount, frequency_label)
      
      # Extra
      plans["plans"][page] << company_2_plan(block_row, value_map["extra"], "extra", "alt3", 
        pliccarfull, ppassfree, ppassdiscount, frequency_label)

      # Contexts
      # Set carrot/stick
      if block_row["carrot_deal"] == 0
        value = block_row["stick"]
        # Rails.logger.debug "SP DEBUG: stick -> #{value}"
        symbol = JsonPath.on(value_map["sticks"], "$..[?(@.value == #{value})]").first["symbol"]

        plans["contexts"][page] = {
          "type" => "stick",
          "measure" => {
            "value" => value,
            "symbol" => symbol
          }
        }
      else
        deal_value = block_row["carrot_deal"]
        # Rails.logger.debug "SP DEBUG: carrot -> #{deal_value}"
        deal_symbol = JsonPath.on(value_map["carrots"]["deals"], "$..[?(@.value == #{deal_value})]").first["symbol"]

        duration_value = block_row["carrot_duration"]
        # Rails.logger.debug "SP DEBUG: carrot -> duration: #{duration_value}"
        duration_symbol = JsonPath.on(value_map["carrots"]["durations"], "$..[?(@.value == #{duration_value})]").first["symbol"]

        plans["contexts"][page] = {
          "type" => "carrot",
          "deal" => {
            "value" => deal_value,
            "symbol" => deal_symbol
          },
          "duration" => {
            "value" => duration_value,
            "symbol" => duration_symbol
          }
        }
      end
    end

    plans
  end

  # Create tourist plan based on block, values and prefix (i.e. type)
  def self.tourist_plan(block_row, block_value_map, prefix)
    plan_elements = block_row.select { |key, value| key.start_with?(prefix) and not key.include?("plan_fee") }

    plan = {
      "modes" => {}
    }

    plan_elements.each do |key, value|
      # mode
      # Rails.logger.debug "SP DEBUG: #{key} -> #{value}"
      mode = key.remove("#{prefix}.")
      mode_hash = JsonPath.on(block_value_map, "$..#{mode}[?(@.value == #{value})]").first

      symbol = mode_hash["symbol"]
      price = mode_hash["price"]

      plan["modes"][mode] = {
        "value" => value,
        "symbol" => symbol,
        "price" => price
      }
    end

    # Calculate price
    multiplier = block_row["#{prefix}.plan_fee"]
    plan["multiplier"] = multiplier

    # base_price & final_price
    base_price = base_price(plan)
    plan["base_price"] = base_price
    plan["final_price"] =  round_to_five(base_price * multiplier)

    plan
  end

  # Create resident company-2 plan
  def self.company_2_plan(block_row, block_value_map, plan_type, prefix, 
    pliccarfull, ppassfree, ppassdiscount, frequency_label)

    case plan_type
    when "basic"
      modes_array = ["public_transport", "bike_sharing"]
    when "urban"
      modes_array = ["public_transport", "bike_sharing", "taxi"]
    when "extra"
      modes_array = ["public_transport", "bike_sharing", "taxi", 
        (pliccarfull == 1) ? "car_sharing" : nil].compact
    end

    plan = {
      "type" => plan_type,
      "frequency_label" => frequency_label,
      "modes" => {}
    }

    # modes
    modes_array.each do |mode|
      value = block_row["#{prefix}.#{mode}"]
      # Rails.logger.debug "SP DEBUG: #{prefix}.#{mode} -> #{value}"
      mode_hash = JsonPath.on(block_value_map, "$..#{mode}[?(@.value == #{value})]").first

      symbol = mode_hash["symbol"]
      price = mode_hash["price"]

      plan["modes"][mode] = {
        "value" => value,
        "symbol" => symbol,
        "price" => price
      }
    end

    # multiplier
    multiplier = block_row["#{prefix}.plan_fee"]
    plan["multiplier"] = multiplier

    # base_price & final_price
    base_price = base_price(plan)
    plan["base_price"] = base_price
    plan["final_price"] =  round_to_five(base_price * multiplier)

    # Additional changes based on type of pass
    if ppassfree == 1
      plan["base_price"] -= plan["modes"]["public_transport"]["price"]
      plan["final_price"] =  round_to_five(plan["base_price"] * plan["multiplier"])
      plan["modes"]["public_transport"]["price"] = 0
    elsif ppassdiscount == 1
      plan["base_price"] -= (plan["modes"]["public_transport"]["price"] * 0.5)
      plan["final_price"] =  round_to_five(plan["base_price"] * plan["multiplier"])
      plan["modes"]["public_transport"]["price"] /= 2
    end

    plan
  end

  # Generates fixed plans for design. # RESIDENT
  # 1, 2: Design - 1 [non-contextual]
  # 3, 4, 5: Design - 1 [contextual]
  # 7, 8, 9: Table Design
  # User.find 29
  def self.design_plans_for_resident(user)
    # Load attributes (based on city)
    city = user.city.name.downcase
    attributes = JSON.parse(IO.read("lib/sp/attributes_resident_#{city}.json"))

    # Obtain feeder values
    # if ppassfree=1, price of all public transport is 0
    ppassfree = user.pre_survey_public_transport_characteristic.ppassfree

    # if ppassdiscount=1, price of all public transport is reduced by 50%
    ppassdiscount = user.pre_survey_public_transport_characteristic.ppassdiscount

    # if pliccarfull=0, Car Sharing cannot be presented (EXTRA)
    pliccarfull = user.pre_survey_private_mobility_characteristic.pliccarfull

    # PLANS TEMPLATE
    plans = {
      "design_1" => {
        "non_contextual" => {},
        "contextual" => {
          "plans" => {},
          "contexts" => {}
        }
      },

      "design_4" => {}
    }

    ## COMPANY - 2 / DESIGN - 1 ##
    # Non-Contextual #
    plans["design_1"]["non_contextual"] = generate_company_2_non_contextual(city, 
      pliccarfull, ppassfree, ppassdiscount)
    Rails.logger.debug "SP DEBUG: Page 1, 2 of Company-2 (NC) done."

    # Contextual #
    plans["design_1"]["contextual"] = generate_company_2_contextual(city, 
      pliccarfull, ppassfree, ppassdiscount)
    Rails.logger.debug "SP DEBUG: Page 1, 2, 3 of Company-2 (C) done."

    ## COMPANY - 1 / DESIGN 4 ##
    plans["design_4"] = generate_company_1(city, pliccarfull)

    plans
  end

  def self.generate_plans_for_page_for_tourists(attributes, plan_modes_map, tliccarfull, ttravelmode)
    plan_1, plan_2, plan_3 = nil

    # Generate plans (for page) in loop until inter_plan_price check passes
    loop do
      # BASIC
      plan_1 = plan(attributes, plan_modes_map.values[0], plan_modes_map.keys[0])
      Rails.logger.debug "SP DEBUG: PLAN 1 done."

      # EASY
      plan_2 = nil
      loop do
        plan_2 = plan(attributes, plan_modes_map.values[1], plan_modes_map.keys[1])

        # 4-limit
        non_null_values = plan_2["modes"].values.map{ |el| el["value"] unless el["value"].zero? }
        non_null_values.compact!
        next if non_null_values.length > 4

        ## SPECIFIC CONDITIONS ##
        null_values = [0, 1, 2, 3]
        if tliccarfull == 0
          # basic_plan_cond is satisfied

          # NOTE: if ttravelmode == 1, then no other NONE
          if ttravelmode == 1
            null_values = [0]
          else
            null_values = [1]
          end
        else
          basic_plan = plan_1["modes"].values.map{ |el| el["value"] }

          if basic_plan == [1, 1, 1] # is pt+bs+bc
            # null_value should be 1
            null_values = [1]
          else
            # null_value should be 1, 2
            null_values = [1, 2]
          end
        end

        # Check null_values
        values = plan_2["modes"].values.map{ |el| el["value"] }
        break if values.count(0).in?(null_values)

        # Reset plan_2
        Rails.logger.debug "SP DEBUG: ... generating plan_2 again."
        plan_2 = nil
      end
      Rails.logger.debug "SP DEBUG: PLAN 2 done."

      # ROAMER
      plan_3 = nil
      loop do
        plan_3 = plan(attributes, plan_modes_map.values[2], plan_modes_map.keys[2], 
          non_null=(tliccarfull == 0))

        # 5-limit
        non_null_values = plan_3["modes"].values.map{ |el| el["value"] unless el["value"].zero? }
        non_null_values.compact!
        next if non_null_values.length > 5

        break if tliccarfull != 1

        ## SPECIFIC CONDITIONS ##
        # If tliccarfull=1, In all experiments, there can only be maximum 1 NONE
        values = plan_3["modes"].values.map{ |el| el["value"] }
        break if values.count(0) <= 1

        # Reset plan_3
        Rails.logger.debug "SP DEBUG: ... generating plan_3 again."
        plan_3 = nil
      end
      Rails.logger.debug "SP DEBUG: PLAN 3 done."

      ## Perform in-page price validations ##
      # 1 < 2 < 3
      break if check_inter_plan_price([plan_1, plan_2, plan_3])

      # Reset plans and re-iterate
    end
    Rails.logger.debug "SP DEBUG: ALL PLANS done."

    [plan_1, plan_2, plan_3]
  end

  def self.select_carrot_or_stick(attributes, phhvehfreq)
    context = {}

    # If frequent driver, pick from carrot/stick
    context_type = "carrot"
    context_type = ["carrot", "stick"].sample if phhvehfreq >= 4

    if context_type == "carrot"
      context = {
        "type" => "carrot",
        "deal" => attributes["carrots"]["deals"].sample,
        "duration" => attributes["carrots"]["durations"].sample
      }
    else
      context = {
        "type" => "stick",
        "measure" => attributes["sticks"].sample
      }
    end

    context
  end

  def self.generate_plans_for_page_for_residents(attributes, feeder_values={})
    basic_plan, urban_plan, extra_plan = nil

    # Generate plans (for page) in loop until inter_plan_price check passes
    loop do
      # BASIC
      basic_plan = basic_plan(attributes)
      # Update prices (if needed) based on pass/discount
      if feeder_values[:ppassfree] == 1
        basic_plan["base_price"] -= basic_plan["modes"]["public_transport"]["price"]
        basic_plan["final_price"] =  round_to_five(basic_plan["base_price"] * basic_plan["multiplier"])
        basic_plan["modes"]["public_transport"]["price"] = 0
      elsif feeder_values[:ppassdiscount] == 1
        basic_plan["base_price"] -= (basic_plan["modes"]["public_transport"]["price"] * 0.5)
        basic_plan["final_price"] =  round_to_five(basic_plan["base_price"] * basic_plan["multiplier"])
        basic_plan["modes"]["public_transport"]["price"] /= 2
      end

      # URBAN
      urban_plan = urban_plan(attributes)
      # Update prices (if needed) based on pass/discount
      if feeder_values[:ppassfree] == 1
        urban_plan["base_price"] -= urban_plan["modes"]["public_transport"]["price"]
        urban_plan["final_price"] =  round_to_five(urban_plan["base_price"] * urban_plan["multiplier"])
        urban_plan["modes"]["public_transport"]["price"] = 0
      elsif feeder_values[:ppassdiscount] == 1
        urban_plan["base_price"] -= (urban_plan["modes"]["public_transport"]["price"] * 0.5)
        urban_plan["final_price"] =  round_to_five(urban_plan["base_price"] * urban_plan["multiplier"])
        urban_plan["modes"]["public_transport"]["price"] /= 2
      end

      # EXTRA
      # NOTE: Send information on URBAN:"3 taxi trips"/option_3
      extra_plan = extra_plan(attributes, (urban_plan["modes"]["taxi"]["value"] == 3), 
        full_licence=(feeder_values[:pliccarfull] == 1))
      # Update prices (if needed) based on pass/discount
      if feeder_values[:ppassfree] == 1
        extra_plan["base_price"] -= extra_plan["modes"]["public_transport"]["price"]
        extra_plan["final_price"] =  round_to_five(extra_plan["base_price"] * extra_plan["multiplier"])
        extra_plan["modes"]["public_transport"]["price"] = 0
      elsif feeder_values[:ppassdiscount] == 1
        extra_plan["base_price"] -= (extra_plan["modes"]["public_transport"]["price"] * 0.5)
        extra_plan["final_price"] =  round_to_five(extra_plan["base_price"] * extra_plan["multiplier"])
        extra_plan["modes"]["public_transport"]["price"] /= 2
      end

      ## Perform in-page price validations ##
      # B < U < E
      break if check_inter_plan_price([basic_plan, urban_plan, extra_plan])

      # Reset plans and re-iterate
    end

    [basic_plan, urban_plan, extra_plan]
  end

  # BASIC plan
  def self.basic_plan(attributes)
    modes = ["public_transport", "bike_sharing"]
    return plan(attributes, modes, "basic")
  end

  # URBAN plan
  def self.urban_plan(attributes)
    modes = ["public_transport", "bike_sharing", "taxi"]
    return plan(attributes, modes, "urban")
  end

  # EXTRA plan
  def self.extra_plan(attributes, urban_plan_flag, full_licence)
    modes = ["public_transport", "bike_sharing", "taxi", full_licence ? "car_sharing" : nil].compact
    plan = nil

    loop do
      plan = plan(attributes, modes, "extra")

      ## SPECIFIC CONDITIONS ##
      # Maximum of 1 NO SHOW, i.e. value=0
      values = plan["modes"].values.map{ |el| el["value"] }
      if values.count(0) <= 1
        # If "3 taxi" is shown in URBAN, check these 3 combos not presented:
        # No taxi + 1 hour of car sharing
        # No taxi + 3 hour of car sharing
        # 3 taxi trips + No car sharing

        if full_licence
          if urban_plan_flag and 
            ((plan["modes"]["taxi"]["value"] == 0 and plan["modes"]["car_sharing"]["value"] == 1) or 
             (plan["modes"]["taxi"]["value"] == 0 and plan["modes"]["car_sharing"]["value"] == 2) or 
             (plan["modes"]["taxi"]["value"] == 3 and plan["modes"]["car_sharing"]["value"] == 0))
            # Re-generate plan
          else
            # Valid plan
            break
          end
        else
          if urban_plan_flag and plan["modes"]["taxi"]["value"] == 3
            # Re-generate plan
          else
            # Valid plan
            break
          end
        end
      end

      # Reset basic_plan to pick modes again
      Rails.logger.debug "SP DEBUG: ... generating EXTRA again."
      plan = nil
    end

    return plan
  end

  # Checks the prices of plans.
  # The array sent as parameter should be in ascending order of base_price / final_price.
  def self.check_inter_plan_price(plans_array)
    base_price_check = plans_array.reduce{ |plan_1, plan_2| plan_1["base_price"] <= 
      plan_2["base_price"] ? plan_2 : (return false) }; true

    # Base Price
    if base_price_check
      final_price_check = plans_array.reduce{ |plan_1, plan_2| plan_1["final_price"] <= 
        plan_2["final_price"] ? plan_2 : (return false) }; true
      # Final Price
      if final_price_check
        return true
      end
    end

    return false
  end

  # Checks that plans _across_ pages don't have the same elements
  def self.check_intra_plan_uniqueness(plan, previous_plans)
    previous_plans.each do |previous_plan|
      (0..2).each do |index|
        return false if (previous_plan[index]["modes"] == plan[index]["modes"])
      end
    end

    true
  end

  def self.check_base_and_final_price_consistency(plans_array)
    all_plans = plans_array.flatten

    all_plans.combination(2).all? { |plan_1, plan_2| 
      (((plan_1["base_price"] <= plan_2["base_price"]) && 
        (plan_1["final_price"] <= plan_2["final_price"])) or 
       ((plan_1["base_price"] > plan_2["base_price"]) && 
        (plan_1["final_price"] > plan_2["final_price"]))) }
  end

  # Generates plan
  def self.plan(attributes, modes_array, plan_type, non_null=false)
    plan = {
      "modes" => {}
    }

    # type
    plan["type"] = plan_type

    # modes
    modes_array.each do |mode|
      options = attributes[plan_type][mode]

      # This is done only for ROAMER plan
      # if tliccarfull = 0, no value=0 should be selected
      # (as car_sharing=0 has been picked already in mode_map)
      if non_null
        Rails.logger.debug "SP DEBUG: Removing null-values from modes"
        options.delete_if {|el| el["value"] == 0 }
      end

      plan["modes"][mode] = options.sample
    end
    
    # multiplier
    multiplier = attributes[plan_type]["multipliers"].sample
    plan["multiplier"] = multiplier

    # base_price & final_price
    base_price = base_price(plan)
    plan["base_price"] = base_price
    plan["final_price"] =  round_to_five(base_price * multiplier)
    plan["frequency_label"] = attributes["frequency_label"]

    plan
  end

  # Calculates the sum of prices of the plan and returns the base_price
  def self.base_price(plan)
    base_price = 0
    plan["modes"].values.each do |plan_mode|
      base_price += plan_mode["price"].to_f
    end

    base_price
  end

  # Rounds value to nearest multiple of 5
  def self.round_to_five(value)
    (value.to_f / 5.0).round * 5
  end

  ## NEW SP METHODOLOGY ##
  def self.symbol_from_value(json, mode, value)
    # JsonPath.on(json["manchester"]["company_1"]["payg"], "$..taxi[?(@.value == 8)]").first["symbol"]
    JsonPath.on(json, "$..#{mode}[?(@.value == #{value})]").first["symbol"]
  end
end