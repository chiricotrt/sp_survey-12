class SectionsController < ApplicationController
  include SectionsHelper

  before_filter :require_user
  before_filter :set_yaml
  before_filter :set_variables

  layout "harmony"

  def next_section
    section_name = post_section
    if section_name.nil?
      redirect_to pages_thankyou_path
    else
      redirect_to section_path(section_name, @parameters)
    end
  end

  def show
    pre_section
  end

  def update
    # update database with submitted answers
    experiment = current_user.sp_experiment
    experiment.responses = {} if experiment.responses.nil?
    section_name = params[:id]
    section_responses = experiment.responses[section_name]
    if section_responses.nil?
      section_responses = experiment.responses[section_name] = {}
    end
    unless @parameters.empty?
      key = []
      @parameters.keys.sort.each do |variable_name|
        value = @parameters[variable_name]
        key.append "#{variable_name}:#{value}"
      end

      key = key.join(',')
      section_responses[key] = {}
      section_responses = section_responses[key]
    end

    accepted_controls = %w[number_field select time_field text_field]
    @yaml.each do |element|
      next unless element.is_a?(Hash)
      if accepted_controls.include?(element.keys[0])
        if element.keys[0] == 'select' && element.values[0]['multiple']
          # multi-response select
          id = element.values[0]['id']
          answers = []
          params.each do |name, value|
            if name.start_with?(id)
              answers.push(value)
            end
          end
          section_responses[id] = answers
        else
          name = element.values[0]['id']
          section_responses[name] = params[name]
        end
      elsif element.keys[0] == 'responses_table'
        element.values[0]['labels_t'].each do |label_t|
          section_responses[label_t] = params[label_t]
        end
      elsif element.keys[0] == 'single_choice_matrix' || element.keys[0] == 'numeric_field_matrix'
        element.values[0]['questions_t'].each do |question_t|
          section_responses[question_t] = params[question_t]
        end
      end
    end

    experiment.locale = I18n.locale
    experiment.save!

    next_section
  end

  private

  def set_yaml
    if params[:id].present?
      if Rails.env.production?
        @yaml = Rails.configuration.x.sections_yaml[params[:id]]
      else
        @yaml = YAML.load_file("config/sections/#{params[:id]}.yml")
      end

      render file: "#{Rails.root}/public/404", layout: false, status: :not_found if @yaml.nil?
    end
  end

end
