## MAIN CALL
jQuery ->
  # Load tooltips
  $(document).tooltip()

  # On selection of "No, ..." to download_maas_app
  $("input[name='download_maas_app']").click ->
    if $("#download_maas_app_3").prop("checked") or $("#download_maas_app_4").prop("checked") or $("#download_maas_app_5").prop("checked")
      $("#reasons_for_not_downloading").show()
    else
      $("input[name='reasons_for_not_downloading[]']").attr('checked',false)
      $("#reasons_for_not_downloading").hide()

  # MOST/LEAST important feature dependencies
  # if MOST selected, LEAST should be disabled (and vice versa)
  $("input[name='most_imporant_features[]']").click ->
    if $(this).prop("checked")
      $("#least_imporant_features_" + $(this).val()).prop("checked", false)
      enforceFeatureSelectionThreshold("least_imporant_features")

  $("input[name='least_imporant_features[]']").click ->
    if $(this).prop("checked")
      $("#most_imporant_features_" + $(this).val()).prop("checked", false)
      enforceFeatureSelectionThreshold("most_imporant_features")

  # if 3 selected, disable the others in the column
  $("input[name='most_imporant_features[]']").click ->
    enforceFeatureSelectionThreshold("most_imporant_features")

  $("input[name='least_imporant_features[]']").click ->
    enforceFeatureSelectionThreshold("least_imporant_features")

  $("#create_experiment").on 'submit', () ->
    # Validations
    # if download_maas_app not picked
    if not $("input[name='download_maas_app']:radio:checked").val()
      $("#download_maas_app_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#download_maas_app_error').offset().top)
      return false

    # if reasons_for_not_downloading not picked [only if download_maas_app=No]
    if ($("#download_maas_app_3").prop("checked") or $("#download_maas_app_4").prop("checked")) and $("input[name='reasons_for_not_downloading[]']:checked").length == 0
      $("#reasons_for_not_downloading_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#reasons_for_not_downloading_error').offset().top)
      return false

    # if 3 MOST/LEAST important features not selected ...
    if $("input[name='most_imporant_features[]']:checked").length != 3 or $("input[name='least_imporant_features[]']:checked").length != 3
      $("#important_features_error").show().delay(5000).fadeOut()
      return false

  # On selection of NONE, ask questions why
  $("input[name='picked_plan']").click ->
    $("#reasons_for_not_choosing").toggle($(this).val() == "4")

  # If OTHER selected, ask for the reason
  $("#reasons_for_not_choosing_8").click ->
    $("#other_reason").toggle($(this).prop("checked"))

  # On selection of "I don't want to use the service", show the reasons
  $("input[name='buy_chosen_plan']").click ->
    if $("#buy_chosen_plan_2").prop("checked")
      $("#reasons_for_not_using").show()
    else
      $("input[name='reasons_for_not_using[]']").attr('checked', false)
      $("#reasons_for_not_using").hide()

  # On selection of "Other", ask for other plan details
  $("#reasons_for_not_using_7").click ->
    if $(this).prop("checked")
      $("#other_plan_section").show()
    else
      $("#other_plan").val("")
      $("#other_plan_section").hide()

  # On submit of form#select_plans [pages>1]
  $('#select_plans').on 'submit', () ->
    # if plan not picked
    if not $("input[name='picked_plan']:radio:checked").val()
      $("#displayed_plans_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#displayed_plans_error').offset().top)
      return false

    # if buy_chosen_plan not selected
    if $("input[name='buy_chosen_plan']").length > 0 and not $("input[name='buy_chosen_plan']:radio:checked").val()
      $("#buy_chosen_plan_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#buy_chosen_plan_error').offset().top)
      return false

    # if reasons_for_not_using not selected (only if buy_chosen_plan = 2)
    if $("#buy_chosen_plan_2").prop("checked") and $("input[name='reasons_for_not_using[]']:checked").length == 0
      $("#reasons_for_not_using_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#reasons_for_not_using_error').offset().top)
      return false

    # if reasons_for_not_using=other and no value for other_plan
    if $("#reasons_for_not_using_7").prop("checked") and !$("#other_plan").val()
      # console.log "Enter value for other plan!"
      $("#other_plan_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#other_plan_error').offset().top)
      return false

    # if None chosen, check if at least one reason is selected
    if $("#picked_plan_4").prop("checked") and $("input[name='reasons_for_not_choosing[]']:checked").length == 0
      $("#reasons_for_not_choosing_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#reasons_for_not_choosing_error').offset().top)
      return false

    # # if None chosen and other selected, check if value for other is present
    # if $("#reasons_for_not_choosing_7").prop("checked") and !$("#other_reason").val()
    #   $("#other_reason_error").show().delay(5000).fadeOut()
    #   $(window).scrollTop($('#other_reason_error').offset().top)
    #   return false

enforceFeatureSelectionThreshold = (featureClass) ->
  if $("input[name='#{featureClass}[]']:checked").length == 3
    $("input[name='#{featureClass}[]']:not(:checked)").prop("disabled", true)
  else
    $("input[name='#{featureClass}[]']:not(:checked)").prop("disabled", false)