if (typeof console == "undefined") var console = { log: function() {} };

var smart;
if (smart == undefined) {
	smart = {};
}
smart.UndoHandler = function(){
	this.coldStart();
};

smart.UndoHandler.prototype = {

	getInstance: function(){

		if (window.smart == undefined)
			window.smart = {};

		if (window.smart.undoHandlerInstance == undefined)
			window.smart.undoHandlerInstance = new smart.UndoHandler();

		window.smart.undoHandlerInstance.initialize();
		return window.smart.undoHandlerInstance;
	},

	coldStart: function(){
		this.umanager = new UndoManager();
		this.umanager.setCallback(this.onUndoChanged);


		this.stopInfo = {};
	},

	initialize: function(){
		_.bindAll(this, "initialize", "clean", "reload", "onUndoChanged",
						"loadStopInfo",
						"onMarkerValidation", "undoValidateStop", "redoValidateStop",
						"onAddStop", "onRemoveStop", "undoAddStop", "redoAddStop",
						"onTimeInputChange", "getStopInfo", "onMarkerDragEnd", "undoMarkerDrag"	);

		//console.log("initializing UndoHandler");
		jQuery.unsubscribe("timepicker:change");
		//jQuery.subscribe("timepicker:change", this.onTimeInputChange);

		// console.log('subscribing to onAddStop');
		jQuery.unsubscribe("add_stop");
		//jQuery.subscribe("add_stop", this.onAddStop);
		jQuery.unsubscribe("remove_stop");
		//jQuery.subscribe('remove_stop', this.onRemoveStop);

		//jQuery.unsubscribe("marker:dragend");
		//jQuery.subscribe('marker:dragend', this.onMarkerDragEnd);

		jQuery.unsubscribe("stop:validated");
		//jQuery.subscribe('stop:validated', this.onMarkerValidation);

		//undoManager.register(undefined, removeCircle, [id], 'Remove circle', undefined, createCircle, [id, x, y, color], 'Create circle');

	},

	loadStopInfo: function(id, data, force){
		//only if we don't have data we are supposed to fill this up
		if (force || this.stopInfo[id] == undefined){
			console.log('Loaded Stop '+id+' Data to UndoHandler');
			this.stopInfo[id] = data;
		}

	},

	onMarkerValidation: function(e, stopId){

		if (this.stopInfo[stopId] != undefined)
			this.stopInfo[stopId]['validated'] = true;
		console.log('Added validation to marker', this.stopInfo);

		this.umanager.register( this, this.undoValidateStop, [stopId], "Undo validate Stop",
								this, this.redoValidateStop, [stopId], "Redo validate Stop");
	},

	undoValidateStop: function(id){
		new Ajax.Request('/report/validateStop/'+id+'?validate=false', {asynchronous:true, evalScripts:true});
	},

	redoValidateStop: function(id){
		new Ajax.Request('/report/validateStop/'+id+'?validate=true', {asynchronous:true, evalScripts:true});
	},


	undoStopInfoChange: function(index, id, field, old_value){
		console.log("Undo Time Change ", arguments);

		this.stopInfo[index][field] = old_value;

		console.log("bef:",jQuery('#'+field+'_'+id).val());
		jQuery('#'+field+'_'+id).val(old_value).change();
		console.log("aft:",jQuery('#'+field+'_'+id).val());
	},

	redoStopInfoChange: function(index, id, field, new_value){
		this.stopInfo[index][field] = new_value;
		console.log("redo "+id+" in field with "+new_value, '#'+field+'_'+id);
		jQuery('#'+field+'_'+id).val(new_value).change();
	},

	onAddStop: function(e, id){
		console.log(arguments, "registering stop with id:",id);

		this.umanager.register( this, this.undoAddStop, [id], "Undo add Stop",
								this, this.redoAddStop, [id], "Redo Add Stop");
	},

	onRemoveStop: function(e, id){
		console.log(arguments, "registering stop remove with id:",id);

		this.umanager.register( this, this.redoAddStop, [id, this.getStopInfo(id)], "Undo remove Stop",
								this, this.undoAddStop, [id], "Redo remove Stop");
	},

	undoAddStop: function(id){
		console.log(arguments, "undo stop with id:",id);
		new Ajax.Request('/report/removeStop/'+id+'?redo=true', {asynchronous:true, evalScripts:true});
	},

	redoAddStop: function( id, tstopInfo ){
		//stopInfo = this.stopInfo[id];
		var validatedStr = "";
		if (tstopInfo != undefined)
			validatedStr = "&validated="+tstopInfo['validated'];

		console.log(tstopInfo);
		console.log(arguments, "redo stop with id:",id )
		new Ajax.Request('/report/insertStop/'+id+'?counter=&redo=true'+validatedStr, {asynchronous:true, evalScripts:true, parameters:'center='+encodeURIComponent(map.getCenter())});
	},


	onTimeInputChange: function(e, target, old_data, new_data){

		var par = jQuery(target).parent();

		var type;
		if (par.hasClass('start_time_input'))
			type = 'start';
		else if (par.hasClass('end_time_input'))
			type = 'end';
		else
			return;

		var id = parseInt(jQuery('input', jQuery(par).parent()).attr('id').split('_')[2],10);
		var index = parseInt(jQuery('.stop_real_id',jQuery("#"+id)).val(),10);


		//#########################
		console.log("get oldData at ", index, this.stopInfo);
		var old_data = this.stopInfo[index][type+"_time"];

		this.stopInfo[index][type+"_time"] = new_data;

		this.umanager.register( this, this.undoStopInfoChange, [index, id, type+"_time", old_data], "Undo Time Change",
								this, this.redoStopInfoChange, [index, id, type+"_time", new_data], "Redo Time Change");
		console.log("registering new undo");

	},

	clean: function(){
		// console.log("clearing undoManager");
		this.umanager.clear();
	},

	reload: function(stops_count, force){
		return;

		if (force == undefined)
			force = false;

		console.log("reloading uManager");
		var that = this;

		var stepid = "#step2";
		if (jQuery("#step2").length == 0)
			stepid = "#step1";

		if (jQuery('.btnUndo').length == 0)
			jQuery('.control_holder').append('<input type="button" class="btnUndo" value="Undo" style=""/>');
		if (jQuery('.btnRedo').length == 0)
			jQuery('.control_holder').append('<input type="button" class="btnRedo" value="Redo" style=""/>');

		jQuery('.btnUndo').unbind('click');
		jQuery('.btnUndo').bind('click', function(){ console.log("################### UNDO ##############"); that.umanager.undo(); });
		jQuery('.btnRedo').unbind('click');
		jQuery('.btnRedo').bind('click', function(){ console.log("################### REDO ##############"); that.umanager.redo(); });

		/*jQuery('.timepicker_container select').unbind('change', this.onTimeInputChange);
		jQuery('.timepicker_container select').bind('change', this.onTimeInputChange);*/

		this.onUndoChanged(this.umanager);

		// Load initial data into backup
		var start_time;
		var end_time;
		var stop_info;
		var real_id;

		var stop_ids = jQuery('.stop_real_id');

		console.log("Reloading Stop Data to UndoHandler");

		stop_ids.each(function(count, e){
			var stop_id = parseInt(jQuery(e).val(),10);
			start_time = jQuery('#start_time_'+stop_id).val();
			end_time = jQuery('#end_time_'+stop_id).val();

			if (start_time == undefined && end_time == undefined){ // marker deleted
				console.log("Skipping");
				return;
			}


			stop_info = that.getStopInfo(stop_id);

			/*
			stop_info.id = stop_id;
			stop_info.start_time = start_time;
			stop_info.end_time = end_time;*/

			that.loadStopInfo(stop_id, stop_info, force);
		});



		//console.log("start: ",start_time, "end: ", end_time, "id:", real_id, "info", stop_info);
	},

	getStopInfo: function(id){
		start_time = jQuery('#start_time_'+id).val();
		end_time = jQuery('#end_time_'+id).val();

		if (start_time == undefined && end_time == undefined){ // marker deleted
			console.log("Skipping");
			return;
		}

		var stoplat = jQuery("#"+id+" .stop_lat").val();
		var stoplng = jQuery("#"+id+" .stop_lng").val();

		var stop_info = getActivityData(id);
		stop_info = jQuery.extend(stop_info, getTravelData(id));

		stop_info.id = id;
		stop_info.start_time = start_time;
		stop_info.end_time = end_time;
		stop_info.lat = stoplat;
		stop_info.lng = stoplng;


		return stop_info;
	},

	onUndoChanged: function(undoManager) {
		jQuery('.btnUndo').attr('disabled', !undoManager.hasUndo());
		jQuery('.btnRedo').attr('disabled', !undoManager.hasRedo());
	},

	undoMarkerDrag: function(id, lat, lng, source_id){
		console.log("UndoMarkerDrag", markers[markersId2Idx[id]]);

		markers[markersId2Idx[id]].setPosition(new google.maps.LatLng(parseFloat(lat), parseFloat(lng)));

		this.stopInfo[id].lat = lat;
		this.stopInfo[id].lng = lng;
		jQuery("#"+id+" .stop_lat").val(lat);
		jQuery("#"+id+" .stop_lng").val(lng);

		new Ajax.Request('/report/saveAfterDrag/?index='+id+'&source_id='+source_id+'&redo=true', {asynchronous:true, evalScripts:true, parameters:'coords='+lat+', '+lng});

	},

	onMarkerDragEnd: function(e, id, lat, lng){
		var sInfo = this.stopInfo[parseInt(id,10)];
		//var old_marker = markers[markersId2Idx[id]].getPosition();
		/*console.log(sInfo, 'old lat:', sInfo.lat, "lng: ",sInfo.lng )
		console.log('new lat:', lat, "lng:", lng);*/

		this.umanager.register( this, this.undoMarkerDrag, [id, sInfo.lat, sInfo.lng, markers[markersId2Idx[id]].source_id], "Undo Marker Move",
								this, this.undoMarkerDrag, [id, lat, lng, 3], "Redo Marker Move");
	}
};