var smart;
if (smart === undefined) {
  smart = {};
}

smart.Diary = {};

smart.Diary.init = function(){

  smart.Diary._activitiesByImage = {};
  smart.Diary._modesByImage = {};
  smart.Diary._activitiesById = {};
  smart.Diary._modesById = {};
  smart.Diary._modesByOrderId = {};

  smart.Diary._validatedCalendarDays = {};
  smart.Diary._suggestedDays = {};
  smart.Diary._calendar = '';
  smart.Diary._currentDay = {};

  smart.Diary._selectedDay = '';

  smart.Diary._formatYMD = 'YYYY-MM-DD';
  smart.Diary._formatDMY = 'D-M-YYYY';

  // smart.Diary.dateFormat = 'yy-mm-dd';
  smart.Diary.dateFormat = 'dd-mm-yy';

  smart.Diary._result = null;

  smart.Diary._survey = {};
  smart.Diary._templates = {};

  jQuery.unsubscribe("activities-fetched", smart.Diary.clearTable);
  jQuery.subscribe("activities-fetched", smart.Diary.renderTable);

  jQuery.unsubscribe("update-main-activity", smart.Diary.updateRowHeader);
  jQuery.subscribe("update-main-activity", smart.Diary.updateRowHeader);

  jQuery.unsubscribe("update-travel-header", smart.Diary.updateRowHeader);
  jQuery.subscribe("update-travel-header", smart.Diary.updateRowHeader);

  jQuery.unsubscribe("update-time-headers", smart.Diary.updateRowHeader);
  jQuery.subscribe("update-time-headers", smart.Diary.updateRowHeader);

  jQuery.unsubscribe("ui:open_row", smart.Diary.showTimepickers);
  jQuery.subscribe("ui:open_row", smart.Diary.showTimepickers);

  smart.Diary._templates.travelRow = "#travel_row";
  smart.Diary._templates.travelInfo = "#travel_info";
  smart.Diary._templates.activityRow = "#activity_row";
  smart.Diary._templates.activityInfo = "#activity_info";
  smart.Diary._templates.overlayMessage = "#overlay_message_template";
}

smart.Diary.updateCalendar = function(date_str){

  if (smart.Diary._calendar !== ''){
    // smart.Diary.locale='';
    jQuery.datepicker.setDefaults( jQuery.datepicker.regional[smart.Diary.locale] );
    jQuery(smart.Diary._calendar).datepicker({
      beforeShowDay: smart.Diary.enableCalendarDays,
      dateFormat: 'D, ' + smart.Diary.dateFormat,
      disabled: false,
      altField: "#datepicker_value",
      altFormat: smart.Diary.dateFormat,
      onSelect: smart.Diary.onDateSelect,
      onChangeMonthYear: smart.Diary.fetchValidatedDays
    });
  }

  if (date_str != null && date_str !== ''){
    var dateParts = date_str.match(/(\d+)/g)
    var realDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
    jQuery('#datepicker').datepicker('setDate', realDate);
    smart.Diary.onDateSelect(realDate);
  }
};

smart.Diary.enableCalendarDays = function(date){
  var today = new Date();

  var dateYMD = moment(date).format(smart.Diary._formatYMD);
  var dateDMY = moment(date).format(smart.Diary._formatDMY);

  if ( smart.Diary._validatedCalendarDays.hasOwnProperty(dateYMD) ) {

    if (smart.Diary._validatedCalendarDays[dateYMD]){
      // return [true, "validated", "Validated"];
      return [true, "validated"];
    }else{
      if (smart.Diary._suggestedDays[dateYMD]){
        // return [true, "suggestedday", "Suggested Day to Validate"];
        return [true, "suggestedday"];
      } else {
        // return [true, "tovalidatewithdata", "Has not been validated"];
        return [true, "tovalidatewithdata"];
      }
    }
  } else if (date <= today){
    // return [true, "tovalidatenonmandatory", "No data"];
    return [true, "tovalidatenonmandatory"];
  } else{
    return [false];
  }
}

smart.Diary.fetchValidatedDays = function(year, month, inst) {
  jQuery.ajax({
    url: "/report/validated_days",
    type: 'GET',
    data: { "year": year, "month": month },
    dataType: "json",
    success: function(result) {
      jQuery.extend(smart.Diary._validatedCalendarDays, result);
      jQuery('#datepicker').datepicker('refresh');
    }
  });
}

smart.Diary.onDateSelect = function(dateText, instance){
  jQuery("#flash_messages").html("");

  smart.Map.clearMap();
  smart.Diary.clearTable();

  var dateObj = new Date(jQuery('#datepicker').datepicker('getDate'));
  var dateInString = moment(dateObj).format(smart.Diary._formatYMD);

  smart.Diary._selectedDay = dateInString;
  smart.Diary._dayStartCutoff = moment(dateObj).startOf('day').subtract(1, 'days');
  smart.Diary._dayStart = moment(dateObj).startOf('day');
  smart.Diary._dayEndCutoff = moment(dateObj).endOf('day').add(1, 'days');
  smart.Diary._dayEnd = moment(dateObj).endOf('day');

  if (window.history.pushState){
      window.history.pushState(dateInString, "Activity Diary "+dateInString, "/report/"+dateInString);
  }
  else{
    History.pushState({"State": dateInString}, "Activity Diary "+dateInString, dateInString);
  }

  smart.Diary.fetchDateStops(dateObj);
}

smart.Diary.fetchDateStops = function(date){

  smart.UndoHandler.prototype.getInstance().clean();
  showWaitIndicator();

  var dateYMD = moment(date).format(smart.Diary._formatYMD);
  var dateDMY = moment(date).format(smart.Diary._formatDMY);

  document.title = "London Mobility Survey - Activity Diary on " + dateYMD;

  moment().tz(smart.Diary._sessionTimezone).format();

  jQuery.ajax({
    url: "/report/get_all_stops",
    type: 'GET',
    data: { "day": dateYMD },
    success: function(result) {
      smart.Diary._currentDay = result;
      hideWaitIndicator();
      jQuery.publish("activities-fetched");
      //smart.messageBox.checkIfDayIsValidated();

      // Initialise multi-select
      $(".multi-select").multipleSelect({
        selectAll: false,
        width: 250
      });

      // Show walk-through ONLY if not mobile!
      if (!(/Mobi/.test(navigator.userAgent))) {
        $("#calendar-hint").hide();
        $("#diary-hint").hide();
        $("#start_duration").show();
        $(".sm_sprite").css("zoom", 1.2);
        smart.Diary.startIntro();
      } else {
        $("#calendar-hint").show();
        $("#diary-hint").show();
        $("#start_duration").hide();
        $(".sm_sprite").css("zoom", 1.5);
      }
    }
  });
}

smart.Diary.startIntro = function() {
  var stop_id = $("input[name='stop_real_id']:eq(1)").val();
  runAccordion("Travel" + stop_id, "travel");

  var intro = introJs();

  intro.setOptions({
    steps: [
      {
        element: '#step1',
        intro: "<h4>First choose the date you would like to inspect by clicking on in it via the calendar.</h4><h5>...</h5>"
      },
      {
        element: '#stoppage_table',
        intro: "<h4>This is your diary that shows the trips and activities you did that day. By clicking verify  you can look at each activity in detail and provide some additional information.</h4>",
        position: 'left',
      },
      {
        element: '#step3',
        intro: "<h4>Sometimes the system may miss some of your stops (for example if you transfer using the tube). You can add these missed stops by clicking on the Add Stop button.</h4>",
        position: 'right',
      },
      {
        element: '.sprite_delete_button',
        intro: "<h4>Other times the system may think you had an activity somewhere when you didn’t (for example if the bus you are on stays at a stop for a long time). You can delete these by clicking on the “x” next to them.</h4>",
        position: 'right',
      },
      {
        element: '#travel_time-' + stop_id,
        intro: "<h4>If you think the time has been misrecorded, you can change the start times of each activity by using these drop down menus</h4>",
        position: 'left',
        tooltipClass: 'screen-shot-intro'
      },
      {
        element: '#map_container',
        intro: "<h4>The map also shows your travel and activities, with the ones you have verified (by hitting submit) turning maroon. Enjoy learning about your day!</h4>",
        position: 'bottom',
        tooltipClass: 'screen-shot-intro'
      }
    ]
  });

  intro.start();

  intro.oncomplete(function() {
    runAccordion("Travel" + stop_id, "travel");
  });

  intro.onexit(function() {
    runAccordion("Travel" + stop_id, "travel");
  });
}

smart.Diary.renderTable = function(e){
  // reset position of #addStopTime before wiping table
  jQuery('#stoppage_table').append(jQuery('#addStopTime'));

  smart.Diary.clearTable();
  resetAccordionOpen();
  smart.Diary.toggleValidationState(true);
  var prevStop = undefined;
  if (smart.Diary._currentDay['stops'].length > 0) {
    jQuery('#stoppage_table_header').show();
    jQuery('#stoppage_table_footer').show();
  }
  else {
    jQuery('#stoppage_table_header').hide();
    jQuery('#stoppage_table_footer').hide();
  }
  jQuery.each(smart.Diary._currentDay['stops'], function(index, value){
    smart.Diary.renderTravelRow(value.stop, index+1, prevStop);
    // console.log("renderTravelRow", index);
    smart.Diary.renderActivityRow(value.stop, index+1, prevStop);
    // console.log("renderActivityRow", index);
    smart.Diary.loadTimepickers(value.stop, prevStop, (index==(smart.Diary._currentDay['stops'].length -1)));
    prevStop = value.stop;
  });

  if (smart.Diary._currentDay['stops'].length > 0) {
    smart.Diary.renderTravelRow(undefined, 0, prevStop);
    smart.Diary.loadTimepickers({"id":0}, prevStop, true);
  }

  smart.Diary.showHappinessQuestions();
  smart.Diary.showMetrics();

  // var adTable = jQuery("#AccordionContainer");
  var adTable = jQuery("#stoppage_table");
  adTable.jqoteapp(smart.Diary._templates.overlayMessage, {}, '*');

  jQuery("[id^=no_persons_]").change(function(e){
    var target = jQuery(e.target);
    var id = target.attr('id');
    id_parts = id.split("_");

    smart.Diary.renderTravelQuestions(parseInt(id_parts[2], 10));
  });

  jQuery('.save_button').click(function(e){
    smart.Diary.clearMessages();

    var target = jQuery(e.target);
    var id = target.attr('rel');

    // todo: should generalize activity/travel rows and assign a common class for dom targeting
    var topEle = target.closest('form');
    var stopId = topEle.attr('id');
    var stopTimeId = stopId;

    var stopType = "activity";
    if (topEle.data().stopType === "travel") {
      stopType = "travel";
      stopTimeId = topEle.data().stopId; // endtime of last stop
    }
    // console.log("saving", stopId, stopTimeId, stopType);
    var time_row = smart.Diary.getStopRowFromStopId(parseInt(stopTimeId,10));
    if (time_row["stop"].stop.canSaveTime !== true) {
      console.log("cannot save time"); //, time_row);
    } else if (time_row) {
      console.log("can save", stopTimeId, stopType);
      smart.Diary.saveStopTime(parseInt(stopTimeId, 10), stopType);
      smart.Diary._currentDay["stops"][time_row["row"]].stop.canSaveTime = false;
    }
    if (stopId !== "Travel0") {
      smart.Diary.saveStopInfo(stopId, stopType, false);
    }
    else {
      runAccordion("Travel0", "travel");
    }
  });


  jQuery('.change_time_button .button').click(function(e){
    if (typeof(ga) == "function") { ga('send', 'event', 'button', 'click', 'change time'); }
    var topEle = jQuery(e.target).parent().parent().parent().parent();
    var stopId = topEle.attr('id');
    var stopType = "activity";
    if (topEle.data().stopType === "travel") {
      stopId = topEle.data().stopId;
      stopType = "travel";
    }

    smart.Diary.saveStopTime(parseInt(stopId, 10), stopType);
    //saveStopInfo(stopId, false);
  });

  jQuery('.button_layer').click(function(e){
    e.preventDefault();
    return false;
  });

  jQuery(document).bind('mousemove', function(e){
    var elm = jQuery('div#floatingHud:visible');
    if (elm.length == 1){
      elm.css({
        left: e.pageX - 75,
        top: e.pageY - 50
      })
    }
  });

  jQuery('.nearby_marker').hover(function(){}, function(){
    if (typeof(ga) == 'function') { ga('send', 'event', 'button', 'hover', 'nearby_marker'); }
  });

  jQuery('.btnAdd').show();
  jQuery('.second_block').show();
  jQuery('.notice_end').hide();

  jQuery('.btnAdd, .btnCancel').off();
  jQuery('.btnAdd, .btnCancel').on({
    click: function(){
      if (typeof(ga) == "function") {
        ga('send', 'event', 'button', 'click', 'add stop');
      }

      //jQuery('#addStopTime').slideToggle();
      if (smart.Diary._currentDay["stops"].length > 0) {
        var obj = jQuery(this);
        var state = (obj.attr('name') !== "btnAdd");
        smart.Diary.toggleValidationState(state);
      }
      else {
        //smart.Diary.loadAddStopModal(this, false);
        // jQuery('#stoppage_table_header').show();
        jQuery('#addStopTime').slideDown();
        smart.Diary.prefillStopTimes(this, false);
      }
    }
  });


  jQuery('.btnResetStop').off();
  jQuery('.btnResetStop').on({
    click: function(){
      if (confirm('Do you want to reset stops for this day? Stops will be regenerated from raw data with previous validation reset')) {
        jQuery.post('/report/' + smart.Diary._selectedDay + '/reset',
          function(result){
            smart.Diary.showMessages(result);
            redrawMap(result);
        });

        showWaitIndicator();
      }
    }
  });

  jQuery('.btn-add').off();
  jQuery('.btn-add').on({
    click: function(){
      //smart.Diary.loadAddStopModal(this, true);
      var accordionTitle = jQuery(this).closest('.AccordionTitle');
      var addStopForm = jQuery('#addStopTime');
      accordionTitle.after(addStopForm);
      addStopForm.slideDown();
      jQuery(".newstop_warnings").html("");
      smart.Diary.prefillStopTimes(this, true);
    }
  });

  jQuery('.btn-edit, .stop_icon').off();
  jQuery('.btn-edit, .stop_icon').on({
    click: function(){
      console.log("toggle accordion");
      // console.log(this);
      smart.Diary.triggerAccordion(this);
      return false;
    }
  });
  jQuery('#closeAddStop').click(function(){ jQuery('#addStopTime').slideUp(); });

  jQuery('#stoppage_table').off();
  jQuery('#stoppage_table').on({
    scroll: function(){
      jQuery('#stop_overlay').css({top: jQuery(this).scrollTop() });
    }
  });
}


smart.Diary.showValidationErrors = function(validatorResult) {
  var warningPlaceholder = jQuery(".newstop_warnings");
  var warningsTemplate = jQuery("#change_time_warnings");
  warningPlaceholder.html("");
  var isValid = true;
  for(var i=0;i<validatorResult.length;i++){
    console.log("validatorResult:", validatorResult[i]);
    warningPlaceholder.append(warningsTemplate.jqote({msg: validatorResult[i].message}));
    if (validatorResult[i].errorCode != 1020) isValid = false;
  }
}

smart.Diary.loadHandlers = function() {
  jQuery('.setMarkerButton').off('click');
  jQuery('.setMarkerButton').on('click', function(ev){
    if (smart.Map._tempMarker === null) {
      alert('Please set a location first.');
    }
    else{
      if (typeof(ga) == "function") { ga('send', 'event', 'button', 'click', 'create stop'); }


      var mainContainer = '#addStopTime'; //'.add_stop_dialog';

      var shours = jQuery(mainContainer + ' .start_time_input .timepicker_hours').val();
      var sminutes = jQuery(mainContainer + ' .start_time_input .timepicker_minutes').val();
      var sday = jQuery(mainContainer + ' .start_time_input .timepicker_day').val();

      var starttime = smart.utils.returnDay(sday)+':'+shours+':'+sminutes;

      var ehours = jQuery(mainContainer + ' .end_time_input .timepicker_hours').val();
      var eminutes = jQuery(mainContainer + ' .end_time_input .timepicker_minutes').val();
      console.log("User entered start time " + starttime + " end hours " + ehours + " end minutes " + eminutes);

      var eday = jQuery(mainContainer + ' .end_time_input .timepicker_day').val();
      var endtime = smart.utils.returnDay(eday)+':'+ehours+':'+eminutes;

      console.log("Lat lon position from map" + smart.Map._tempMarker.getPosition().toString());

      var rowIdx = 0;
      var btnAdd = jQuery(this).closest('.travel_form').find('.btn-add');
      if (btnAdd.length > 0) rowIdx = btnAdd.data().rowIdx;

      var tempPos = smart.Map._tempMarker.getPosition();

      var tempStop = {
        'start_time': starttime,
        'end_time': endtime,
        'stop_lng': tempPos.lng(),
        'stop_lat': tempPos.lat()
      }

      if (true !== smart.Diary.validateNewStop(rowIdx, tempStop)) {
        return false;
      }

      var newStopParams = {
        center: smart.Map._tempMarker.getPosition().toString(),
        starttime: starttime,
        endtime: endtime,
        current_day: smart.Diary._selectedDay,
        address: smart.Map._tempLocationInput.value
      }

      jQuery.post('/report/insertNewStop',
        newStopParams,
        function(result){
          //jQuery('.ink-dismiss').click();
          smart.Diary.showMessages(result);

          // insertion after last stop row
          if (rowIdx === -1) {
            result["redraw"] = true;
            result["day"] = smart.Diary._selectedDay;
          }
          redrawMap(result);
      });

      showWaitIndicator();
      jQuery('#addStopTime').slideUp();
    }
  });

  jQuery('.btn-showtraces').off('click');
  jQuery('.btn-showtraces').on('click', function(ev){
    updateTraces();
  });
}

smart.Diary.validateNewStop = function(rowIdx, tempStop) {
  var previousStop = null;
  var stopsCount = smart.Diary._currentDay['stops'].length;

  if (rowIdx > 0) {
    previousStop = smart.utils.buildValidatorObject(smart.Diary._currentDay['stops'][rowIdx-1].stop, {});
  }
  else if (rowIdx === -1) {
    previousStop = smart.utils.buildValidatorObject(smart.Diary._currentDay['stops'][stopsCount-1].stop, {});
  }
  var nextStop = null;

  // not the last row and not the only stop available
  if (rowIdx !== -1 && rowIdx < stopsCount) {
    nextStop = smart.utils.buildValidatorObject(smart.Diary._currentDay['stops'][rowIdx].stop, {});
  }
  var currentStop = tempStop;

  var validatorResult;

  validatorResult = smart.ad_validator.run(["start_time", "end_time"], currentStop, previousStop, nextStop);
  // invalid stop times
  if (_.find(validatorResult, function(elm){
    return (elm.errorCode >= 1000 && elm.errorCode <= 1003);
  })) {
    validatorResult = [
    {
      'errorCode': 2000,
      'message': 'New stop has invalid start/end times or zero duration, please confirm they do not overlap with the preceding or following stop.'
    }
    ];
    smart.Diary.showValidationErrors(validatorResult);
    return false;
  }

  if (rowIdx > 0) {
    validatorResult = smart.ad_validator.run(['activity_location'], currentStop, previousStop, nextStop);
    // impossible speed
    if (_.find(validatorResult, function(elm){ return elm.errorCode === 1011 || elm.errorCode === 1010; })) {
      validatorResult = [
        {
          'errorCode': 2010,
          'message': 'New stop is too far from neighboring stops, please confirm location again.'
        }
      ];
      smart.Diary.showValidationErrors(validatorResult);
      return false;
    }
  }

  // var d1 = smart.Map.getDistance(prevMarker, tempMarker);
  // var d2 = smart.Map.getDistance(nextMarker, tempMarker);
  // alert("Distance from neighboring stops: " + d1 + " " + d2);
  return true;
}

smart.Diary.prefillStopTimes = function(element, hasOtherStops) {
  var start_limit = smart.Diary._dayStart;
  var end_limit = smart.Diary._dayEnd;



  if (hasOtherStops === true) {
    var data = jQuery(element).data();
    var rowIdx = data['rowIdx'];

    var prevStop = undefined;
    var nextStop = undefined;

    if (rowIdx > 0) {
      prevStop = smart.Diary._currentDay['stops'][rowIdx-1].stop;
      var prevMarker = smart.Map._markers[rowIdx-1].getPosition();
      var nextMarker = smart.Map._markers[rowIdx].getPosition();
      var inBetween = google.maps.geometry.spherical.interpolate(prevMarker, nextMarker, 0.5);
      smart.Diary.addMarkerToModalMap(inBetween, smart.Map._map);
      // console.log("between markers:", rowIdx, prevMarker, nextMarker, inBetween);
    }
    if (rowIdx < 0) { // dummy travel row
      var stopsCount = smart.Diary._currentDay['stops'].length;
      prevStop = smart.Diary._currentDay['stops'][stopsCount-1].stop;
    }
    else {
      nextStop = smart.Diary._currentDay['stops'][rowIdx].stop;
    }

    if (prevStop !== undefined) {
      start_limit = smart.utils.getMomentFromTimestamp(prevStop.endtime);
    }
    start_limit.add(1, 'minutes');

    if (nextStop !== undefined) {
      end_limit = smart.utils.getMomentFromTimestamp(nextStop.starttime);
    }
    end_limit.subtract(1, 'minutes');

    if (end_limit.isBefore(start_limit)) {
      if (prevStop !== undefined) {
        end_limit = start_limit;
      }
      else {
        start_limit = end_limit;
      }
    }
    if (prevStop !== undefined) {
      prevStopTime = smart.utils.parseStopTime(prevStop.endtime, smart.Diary._selectedDay);
      prevStopActivity = smart.Diary._activitiesByImage[prevStop.main_activity].name;
    }

    if (nextStop !== undefined) {
      nextStopTime = smart.utils.parseStopTime(nextStop.starttime, smart.Diary._selectedDay);
      nextStopActivity = smart.Diary._activitiesByImage[nextStop.main_activity].name;
    }
  }

  jQuery('#addStopTime .start_time_input .timepicker_hours').val(start_limit.format('HH'));
  jQuery('#addStopTime .start_time_input .timepicker_minutes').val(start_limit.format('mm'));

  jQuery('#addStopTime .end_time_input .timepicker_hours').val(end_limit.format('HH'));
  jQuery('#addStopTime .end_time_input .timepicker_minutes').val(end_limit.format('mm'));

  var start_day_flag = '0';
  var end_day_flag = '0';
  if (start_limit.isBefore(smart.Diary._dayStart)) {
    start_day_flag = '-1';
  }
  if (smart.Diary._dayEnd.isBefore(end_limit)) {
    end_day_flag = '+1';
  }

  jQuery('#addStopTime .start_time_input .timepicker_day').val(start_day_flag);
  jQuery('#addStopTime .end_time_input .timepicker_day').val(end_day_flag);
}

smart.Diary.toggleValidationState = function(state){
  var add_label = I18n.t("report.add_activity");
  var validate_label = I18n.t('generic.cancel');
  var btnAdd = jQuery('.btnAdd');
  var btnCancel = jQuery('.btnCancel');

  if(state === true) {
    // jQuery('.travel_form .btn-edit').text('Check');
    jQuery('.travel_form .btn-edit').show();
    jQuery('.travel_form .btn-add').hide();
    jQuery('.stop_form .btn-edit').show();

    btnCancel.hide();
    btnAdd.show();

    jQuery('#addStopTime').hide();
  }
  else {
    jQuery('.travel_form .btn-edit').hide();
    jQuery('.travel_form .btn-add').show();
    jQuery('.stop_form .btn-edit').hide();

    btnAdd.hide();
    btnCancel.show();
  }

  smart.Map.resetTempMarker();
}

smart.Diary.addMarkerToModalMap = function(position, map) {
  if (smart.Map._tempMarker !== null) smart.Map._tempMarker.setMap(null);

  var marker = new MarkerWithLabel({
    position: position,
    map: map,
    draggable: true,
    raiseOnDrag: true,
    icon: '/assets/smart/markers/activity-default-checked.png',
    shadow: '/assets/shadow.png',
    // labelContent: ''+counter,
    labelAnchor: new google.maps.Point(-5,43),
    labelClass: 'markerlabels',
    labelStyle: {opacity: 1}
  });

  google.maps.event.addListener(marker, 'dragend', function () {
    smart.Map._geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        var input = document.getElementById('mapSearch');
        input.value = results[0].formatted_address;
      }
    });
  });

  map.setCenter(position);
  smart.Map._tempMarker = marker;

  // google.maps.event.trigger(marker, 'dragend', {'latLng': position});
}

smart.Diary.setDefaultMarker = function(map, location){
  var marker;
  if ('undefined' === typeof location) {
    // default to create way
    marker = new google.maps.LatLng(1.303904, 103.773827);
  } else {
    marker = new google.maps.LatLng(location.latitude, location.longitude);
  }
  smart.Diary.addMarkerToModalMap(marker, map);
}


smart.Diary.saveStopTime = function(stopId, stopType){
  var timeType = "start";
  if (stopType === "travel") {
    timeType = "end";
  }

  var timeObj = jQuery("#"+timeType+"_time_"+stopId);
  var timeInTimezone = smart.utils.buildTimestampWithTimezone(timeObj.val());
  var timeKey = timeType+"time";

  var input_hash = {
    "stop_id": stopId,
    "current_day": smart.Diary._selectedDay
  };
  input_hash[timeKey] = timeInTimezone;


  jQuery.ajax({
    url: "/report/updateStopTime",
    type: 'POST',
    data: input_hash,
    success: function(result) {

      var stop_contents = smart.Diary.getStopRowFromStopId(stopId);
      var updated_stop_info = result["stop"];

      stop_contents["stop"].stop.starttime = updated_stop_info.stop["starttime"];
      stop_contents["stop"].stop.endtime = updated_stop_info.stop["endtime"];
      stop_contents["stop"].stop.source_id = updated_stop_info.stop["source_id"];

      smart.Diary.showMessages(result);
              if (result["message"]["class"] !== "content_feedback") return;

      //hide button
      jQuery('#'+stopId+' .change_time_button').hide();

      if (result["redraw"] === true){
        //sort the activities
        smart.Diary._currentDay['stops'] = smart.Diary._currentDay['stops'].sort(smart.utils.compareStopTime);
        jQuery.publish("activities-fetched");
      }
      else{
        var stop = stop_contents["stop"].stop;
        // console.log(stopType, stop_contents);
        var row = stop_contents["row"];

        var thisStop = smart.Diary._currentDay['stops'][row].stop;
        var activityDuration = smart.utils.getStopDuration(thisStop.starttime, thisStop.endtime);

        update_hash =  {
          "stop_id": stopId,
          "activity_duration": activityDuration
        }

        if (stopType === "travel") {
          if (row < smart.Diary._currentDay['stops'].length -1) {
            var nextStop = smart.Diary._currentDay['stops'][row+1].stop;
            update_hash["next_duration"] = smart.utils.getStopDuration(thisStop.endtime, nextStop.starttime);
          }
        }
        else if (stopType === "activity") {
          if (row > 0) {
            var prevStop = smart.Diary._currentDay['stops'][row-1].stop;
            update_hash["prev_duration"] = smart.utils.getStopDuration(prevStop.endtime, thisStop.starttime);
          }
        }

        update_hash["starttime_header"] = smart.utils.parseStopTime(stop.starttime, smart.Diary._selectedDay);
        update_hash["endtime_header"] = smart.utils.parseStopTime(stop.endtime, smart.Diary._selectedDay);

        jQuery.publish("update-time-headers", update_hash);
      }
    }
  });
}

smart.Diary.saveStopInfo = function(stopRowId, stopType){
  var stopData;
  var stopId = stopRowId;

  //if activity
  if (stopType !== "travel") {
    stopData = smart.Diary.getActivityData(parseInt(stopId, 10));
    stopData['isActivity'] = true;
    stopData['stop_id'] = stopId;
  } else {
    stopId = (""+stopRowId).match(/^Travel(.*)/)[1];

    if (parseInt(stopId,10) === 0) {
      // this is the dummy travel row, only save time
      smart.Diary.saveStopTime(0,'travel');
    } else {
      stopData = smart.Diary.getTravelData(parseInt(stopId, 10));
      stopData['isActivity'] = false;
      stopData['stop_id'] = stopId; // this contains the numeric value; stopId is prefixed by 'travel'

      if (stopData['mode'] != 'mode-default') {
        var doTravel = true;
        setTravelValidatedStatus(stopId, doTravel);
      }
    }
  }
  // previously updateStopInfo is called and it is a huge mess,
  // and not all previous functionality has been retained
  // after the switch to saveStopInfo, still missing:
  // 1) update of stop.validated (done with slightly different logic)
  // 2) display of validation messages (some of the previous implementation has not been added
  // to the new validator)

  stopData["current_day"] = smart.Diary._selectedDay;

  jQuery.post('/report/saveStopInfo', stopData)
    .done(function(answer) {
      if (answer['synched']==false){
        jQuery('#stop_overlay .overlay_message').html(jQuery('#out_of_sync_msg').jqote());
        jQuery('#stop_overlay').show();
      }
      else{
        redrawMap(answer);
        console.log("Validated? " + answer["validated_flag"]);
        if ((answer.stop.stop.address != jQuery("#stop_" + stopId + "_address").text()) && answer.stop.stop.address != null) {
          jQuery("#stop_" + stopId + "_address").text(answer.stop.stop.address); // Redraw address if changed
        };

        if (stopType !== "travel") {
          var row_info = smart.Diary.getStopRowFromStopId(parseInt(stopId, 10));
          jQuery.publish("update-main-activity", {"stop_id": stopId, "main_activity": answer["main_activity"], "row": row_info["row"], "status": answer["validated_flag"]});

          var doActivity = (answer["validated_flag"] & 0x1) > 0;
          setActivityValidatedStatus(stopId, doActivity);
        }
        smart.Diary.openNextAccordion();
      }
    }
  );
}

smart.Diary.getActivityData = function(stop_id){
  //return (jQuery( "#"+stop_id ).serialize());
  stop_contents = smart.Diary.getStopRowFromStopId(stop_id);

  var retObj = {
    index: stop_id,
    mainactivity: stop_contents["stop"].stop.main_activity,
    activities: stop_contents["stop"].stop.user_activities,
    validated: stop_contents["stop"].stop.validated,
    start_time: stop_contents["stop"].stop.starttime,
    end_time: stop_contents["stop"].stop.endtime,
  };

  retObj = jQuery.extend(retObj, jQuery("#"+stop_id).serializeObject());

  return retObj;
}

smart.Diary.getTravelForm = function(stop_id){
  return jQuery("#Travel"+stop_id);
}

smart.Diary.getTravelData = function(stop_id){
  stop_contents = smart.Diary.getStopRowFromStopId(stop_id);

  var retObj =  {
    index: stop_id,
    mode: smart.Diary.getModeKey(stop_contents["stop"].stop.mode_id),
    validated: stop_contents["stop"].validated
  };

  retObj = jQuery.extend(retObj, smart.Diary.getTravelForm(stop_id).serializeObject());
  console.log("captured traveldata", retObj);

  return retObj;
}

smart.Diary.openNextAccordion = function(){
  var open_accordion = whichAccordionIsOpen();
  //find current row
  var stop_contents = smart.Diary.getStopRowFromStopId(parseInt(open_accordion, 10));
  var current_idx = stop_contents["row"];

  if (isRowTravel()){
    //open activity with same id
    //runAccordion(open_accordion);
    smart.Diary.toggleAccordion(current_idx,"activity");
  }
  else{
    //get next id
    var idx = current_idx+1;
    if (idx < smart.Diary._currentDay['stops'].length){
      var next_id = smart.Diary._currentDay['stops'][idx].stop.id;
      //open correspondent travel
      //runAccordion("Travel"+next_id);
      smart.Diary.toggleAccordion(idx,"travel");
    }
    else{
      //close current accordion
      runAccordion(open_accordion, "travel");
    }
  }
}



smart.Diary.deleteActivitySequence = function(act_id, counter){
  if (confirm('Do you really want to delete activity '+counter+'?')) {
    showWaitIndicator();
    smart.NearbyLocations.unload();
    if (typeof(ga) == 'function') {
      ga('send', 'event', 'button', 'click', 'delete stop');
    };

    jQuery.ajax({
      url: "removeStop",
      type: 'GET',
      data: {
        "index": act_id,
        "start_time": smart.Diary._currentDay["stops"][counter-1].stop.starttime,
        "current_day": smart.Diary._selectedDay
      },
      success: function(result) {

        validateSync(result);
        if (result.message === 'success'){
          smart.Diary._currentDay['stops'].splice(counter-1, 1);
          smart.Diary.renderTable();
        }

        redrawMap(result);
      }
    });
  }
}

smart.Diary.deleteTravelSequence = function(act_id, counter){
  if (confirm('Do you really want to remove this travel segment? The preceding and next stops will be merged.')) {
    if (typeof(ga) == 'function') {
      ga('send', 'event', 'button', 'click', 'merge stop');
    };

    var prevStop = smart.Map._markers[counter-2].getPosition();
    var nextStop = smart.Map._markers[counter-1].getPosition();
    var dist = calcDistance(prevStop, nextStop);

    if (dist > 0.15) {
      alert("Stops are too far apart, so this segment cannot be deleted.");
      return;
    }

    showWaitIndicator();
    smart.NearbyLocations.unload();

    jQuery.ajax({
      url: "mergeStop",
      type: 'GET',
      data: {
        "index": act_id,
        "start_time": smart.Diary._currentDay["stops"][counter-1].stop.starttime,
        "current_day": smart.Diary._selectedDay
      },
      success: function(result) {

        validateSync(result);
        if (result.message === 'success'){
          smart.Diary._currentDay['stops'].splice(counter-1, 1);
          smart.Diary.renderTable();
        }

        redrawMap(result);
      }
    });
  }
}

smart.Diary.renderActivityRow = function(act, counter, prevStop){

  var activityTableRowTemplate = jQuery("#activity-table-row");
  var adTable = jQuery("#AccordionContainer");

  adTable.jqoteapp(smart.Diary._templates.activityRow, {act: act, counter:counter}, '*');

  jQuery("#Accordion"+act.id+"Content").jqoteapp(
    smart.Diary._templates.activityInfo,
    {act: act, counter: counter, questions: smart.Diary._survey.activity}, '*'
  );

  //console.log(index, smart.Diary._currentDay.length - 1);
  //counter = index + 1
  //number shown in the table, therefore the comparison with lenght and not length - 1
  // smart.Diary.loadTimepickers(act, prevStop, counter==smart.Diary._currentDay.length);
}

smart.Diary.renderTravelRow = function(act, index, prevStop){

  var travelTableRowTemplate = jQuery("#travel-table-row");
  var adTable = jQuery("#AccordionContainer");

  if (prevStop === undefined){
    return;
  }

  var infoTemplate = smart.Diary._templates.travelInfo;
  var thisStop = act;
  if (thisStop === undefined) {
    // we need to stub thisStop
    thisStop = {
      "id" : 0,
      "mode_id" : -1,
      "validated" : -1
    };
    thisStop["starttime"] = prevStop.endtime;
    infoTemplate = "#dummy_travel_info";
  }

  adTable.jqoteapp(smart.Diary._templates.travelRow, {act: thisStop, counter:index, prevStop: prevStop}, '*');
  jQuery("#AccordionTravel"+thisStop.id+"Content").jqoteapp(
    infoTemplate,
    {act: thisStop, questions: smart.Diary._survey.travel, prevStop: prevStop}, '*'
  );

  // Set travel details
  smart.Diary.updateTravelDetails(thisStop);
}

smart.Diary.updateTravelDetails = function(stop) {
  // console.log("mode -> " + stop.mode_name);
  switch(stop.mode_name) {
    case "mode-private_vehicle":
      if (stop.travel_details) {
        $("#travel_details_private_vehicle_vehicle_" + stop.id + "_" + stop.travel_details.vehicle).prop("checked", true);
        $("#travel_details_private_vehicle_driver_" + stop.id + "_" + stop.travel_details.driver).prop("checked", true);
        $("#travel_details_private_vehicle_cost_of_parking_" + stop.id).val(stop.travel_details.cost_of_parking);
        $("#travel_details_private_vehicle_parking_" + stop.id).val(stop.travel_details.parking);
      }
      break;
    case "mode-bus":
      if (stop.travel_details) {
        $("#travel_details_bus_bus_number_" + stop.id).val(stop.travel_details.bus_number);
        $("#travel_details_bus_trip_payment_" + stop.id).val(stop.travel_details.trip_payment);
      }
      break;
    case "mode-tube":
      if (stop.travel_details) {
        $("#travel_details_tube_trip_payment_" + stop.id).val(stop.travel_details.trip_payment);
        $("#travel_details_tube_tube_line_" + stop.id).val(stop.travel_details.tube_line);
      }
      break;
    case "mode-national_rail":
      if (stop.travel_details) {
        // $("#travel_details_national_rail_trip_payment_" + stop.id + "_" + stop.travel_details.trip_payment).prop("checked", true);
        $("#travel_details_national_rail_trip_payment_" + stop.id).val(stop.travel_details.trip_payment);
        $("#travel_details_national_rail_cost_of_trip_" + stop.id).val(stop.travel_details.cost_of_trip);
        $("#travel_details_national_rail_season_ticket_period_" + stop.id).val(stop.travel_details.season_ticket_period);
        $("#travel_details_national_rail_frequency_of_trip_" + stop.id).val(stop.travel_details.frequency_of_trip);
      }
      break;
    case "mode-car_club":
      if (stop.travel_details) {
        $("#travel_details_car_club_driver_" + stop.id + "_" + stop.travel_details.driver).prop("checked", true);
        $("#travel_details_car_club_cost_of_usage_" + stop.id).val(stop.travel_details.cost_of_usage);
      }
      break;
    case "mode-taxi":
      if (stop.travel_details) {
        $("#travel_details_taxi_cost_of_trip_" + stop.id).val(stop.travel_details.cost_of_trip);
        $("#travel_details_taxi_taxi_type_" + stop.id).val(stop.travel_details.taxi_type);
        $("#travel_details_taxi_reimbursable_" + stop.id).prop("checked", stop.travel_details.reimbursable);
      }
      break;
    case "mode-bike":
      if (stop.travel_details) {
        $("#travel_details_bike_safety_" + stop.id).val(stop.travel_details.safety);
      }
      break;
    case "mode-bike_share":
      if (stop.travel_details) {
        $("#travel_details_bike_share_safety_" + stop.id).val(stop.travel_details.safety);
      }
      break;
    case "mode-other":
      if (stop.other_travel_details) {
        $("#travel_details_other_mode_" + stop.id).val(stop.other_travel_details.other_mode);
      }
    default:
      break;
  }

  // Update happiness
  $("#travel_details_happiness_" + stop.id).val(stop.happiness_value);

  // Update travel details (i.e. activity during travel)
  if (stop.other_travel_details) {
    $("#travel_details_activity_during_travel_" + stop.id).val(stop.other_travel_details.activity_during_travel);

    // NOTE: This is not used anymore!
    // smart.Diary.updateActivityMethodOptions($("#travel_details_activity_during_travel_" + stop.id), stop.id);

    // if (stop.other_travel_details.activity_during_travel_method) {
    //   $("#activity_during_travel_method_container_" + stop.id).show();
    // } else {
    //   $("#activity_during_travel_method_container_" + stop.id).hide();
    // }

    // $("#travel_details_activity_during_travel_method_" + stop.id).val(stop.other_travel_details.activity_during_travel_method);
  }
}

smart.Diary.updateNationalRailForm = function(element, stop_id) {
  switch ($(element).val()) {
    case "1": // Single
      $("div#group_" + stop_id + "_mode-national_rail p.return").hide();
      $("div#group_" + stop_id + "_mode-national_rail p.season").hide();
      $("div#group_" + stop_id + "_mode-national_rail div.season_ticket_period").hide();

      $("div#group_" + stop_id + "_mode-national_rail p.single").show();
      break;

    case "2": // Return
      $("div#group_" + stop_id + "_mode-national_rail p.season").hide();
      $("div#group_" + stop_id + "_mode-national_rail p.single").hide();
      $("div#group_" + stop_id + "_mode-national_rail div.season_ticket_period").hide();

      $("div#group_" + stop_id + "_mode-national_rail p.return").show();
      break;

    case "3": // Season
      $("div#group_" + stop_id + "_mode-national_rail p.return").hide();
      $("div#group_" + stop_id + "_mode-national_rail p.single").hide();

      $("div#group_" + stop_id + "_mode-national_rail p.season").show();
      $("div#group_" + stop_id + "_mode-national_rail div.season_ticket_period").show();
      break;
  }
}

// This method is not used anymore!
smart.Diary.updateActivityMethodOptions = function(element, stop_id) {
  $("#travel_details_activity_during_travel_method_" + stop_id).empty();

  switch ($(element).val()) {
    case "5": // Music
      var musicOptions = {
        "Downloaded music": "1",
        "Spotify": "2",
        "YouTube": "3",
        "iTunes": "4",
        "Other": "5"
      };

      $("#activity_during_travel_method_container_" + stop_id).show();
      $.each(musicOptions, function(key, value) {
        $("#travel_details_activity_during_travel_method_" + stop_id).append($("<option></option>").attr("value", value).text(key));
      });
      break;

    case "6": // Movies
      var movieOptions = {
        "Downloaded music": "1",
        "Netflix": "2",
        "YouTube": "3",
        "Other": "4"
      };

      $("#activity_during_travel_method_container_" + stop_id).show();
      $.each(movieOptions, function(key, value) {
        $("#travel_details_activity_during_travel_method_" + stop_id).append($("<option></option>").attr("value", value).text(key));
      });
      break;

    case "7": // Books
      var bookOptions = {
        "Paperback": "1",
        "Kindle": "2",
        "Phone": "3",
        "Other": "4"
      };

      $("#activity_during_travel_method_container_" + stop_id).show();
      $.each(bookOptions, function(key, value) {
        $("#travel_details_activity_during_travel_method_" + stop_id).append($("<option></option>").attr("value", value).text(key));
      });
      break;

    case "8": // Paper
      var paperOptions = {
        "Free paper picked up from station": "1",
        "Paper I bought at the station": "2",
        "Paper I had with me before I arrived to the station": "3"
      };

      $("#activity_during_travel_method_container_" + stop_id).show();
      $.each(paperOptions, function(key, value) {
        $("#travel_details_activity_during_travel_method_" + stop_id).append($("<option></option>").attr("value", value).text(key));
      });
      break;

    default:
      // Hide method options
      $("#activity_during_travel_method_container_" + stop_id).hide();
      break;
  }
}

smart.Diary.clearTable = function(){
  var adTable = jQuery("#AccordionContainer");
  jQuery(adTable).empty();
}

smart.Diary.loadTimepickers = function(act, prevStop, isLast){

  var currentSTime = "0:00";
  var startTimeObj = jQuery("#start_time_"+act.id);

  if (startTimeObj != undefined)
    currentSTime = jQuery(startTimeObj).val();

  var currentETime = "23:59";
  var endTimeId = "undefined";
  if (prevStop != undefined) {
    endTimeId = prevStop.id;
  }
  var entTimeObj = jQuery("#end_time_"+endTimeId);
  if (entTimeObj.length !== 0) {
    currentETime = jQuery(entTimeObj).val();
  }

  var minTime = "0:00";
  var maxTime = "23:59";
  //console.log("i = "+i)
  if (prevStop != undefined){
    //console.log("minTime =",jQuery(endTimeArr[i-1]).val());
    minTime = jQuery("#end_time_"+prevStop.id).val()
  } else {
    currentSTime = "0:00";
  }

  var startOptions = new Object();
  startOptions.hours = {
      starts: 0,//parseInt(""+minTime.split(':')[0],10),
      ends: 23,//parseInt(""+currentETime.split(':')[0],10),
      endHour: currentETime.split(':')[1]
  };
  startOptions.minutes= {
      starts: 0,//parseInt(minTime.split(':')[1],10),
      ends: 59 //parseInt(currentETime.split(':')[1],10),
  }
  startOptions.onClose= timePickerOptions.onClose

  if (prevStop == undefined){
    startOptions.isFirst = true;
  }

  if (startTimeObj.length !== 0){
    startOptions.id= "#"+startTimeObj.attr('id');
    startOptions.classes = "start_time_input"
    new smart.timepicker(startOptions);
  }


  //Endtime timepicker
  var opts2 = new Object();
  opts2.validateTime = false;
  opts2.hours = {
    starts: 0,//parseInt(currentSTime.split(':')[0],10),
    ends: 23//parseInt(maxTime.split(':')[0],10),
  }
  opts2.minutes = {
    //interval: 5
    starts: 0,//parseInt(currentSTime.split(':')[1],10),
    ends: 59//parseInt(maxTime.split(':')[1],10)
  }
  opts2.onClose = timePickerOptions.onClose;
  opts2.isLast = isLast;

  if (entTimeObj.length !== 0){
    opts2.id= "#"+entTimeObj.attr('id');
    opts2.classes = "end_time_input"
    new smart.timepicker(opts2);
  }

  jQuery.unsubscribe("timepicker:change", smart.Diary.checkTimeBeforeChange);
  jQuery.subscribe("timepicker:change", smart.Diary.checkTimeBeforeChange);
}

smart.Diary.triggerAccordion = function(element) {
  var stopForm = jQuery(element).closest('form');
  var data = stopForm.data();
  smart.Diary.toggleAccordion(data['rowIdx'], data['stopType']);

  //jQuery.publish('center-map', {'row':data['rowIdx'], 'type':data['stopType']});
}

smart.Diary.toggleAccordion = function(selectedRow, travel_activity){
  //verify if there is any accordion currently opened
  //if yes, then send the current data to the server
  //close current accordion
  //end if
  //fetch answers of current stop
  if (selectedRow >= 0 && selectedRow < smart.Diary._currentDay['stops'].length) {
    smart.Diary.fetchStopAnswers(selectedRow, travel_activity);
    jQuery.publish("center-map", {"row":selectedRow, "type":travel_activity});
  }
  else{
    runAccordion("Travel0", "travel");
  }

  if (travel_activity === "activity"){
    smart.NearbyLocations.unload();
    smart.NearbyLocations.load(smart.Diary._currentDay['stops'][selectedRow].stop.id);
  }
}

smart.Diary.fetchStopAnswers = function(row, stop_type){
  var stop_id = smart.Diary._currentDay['stops'][row].stop.id;
  jQuery.ajax({
    url: "/useranswers/diary/"+stop_id,
    type: 'GET',
    success: function(result) {
      // console.log("user_answers for " + stop_id,result);

      if (stop_type === "activity"){
        smart.Diary.updateActivityContent(row);
        stop_id = stop_id;
      }
      else if(stop_type === "travel"){
        smart.Diary.updateTravelContent(row);
        stop_id = "Travel"+stop_id;
      }
      else{
        console.log("wrong parameters");
      }

      //open accordion
      runAccordion(stop_id, stop_type);
    }
  });
}

smart.Diary.updateTravelContent = function(row){
  var stop_to_load = smart.Diary._currentDay['stops'][row].stop;
  //smart.
  smart.Diary.selectMode(stop_to_load.mode_id, stop_to_load.id);
  smart.Diary.renderQuestions(stop_to_load.id, "travel");
}

smart.Diary.updateActivityContent = function(row){

  //console.log("UPDATE ACTIVITY CONTENT: " +row);

  var stop_to_load = smart.Diary._currentDay['stops'][row].stop;
  var i;
  var total_activities = stop_to_load.user_activities.length;
  //update activities list
  for(i=0; i<total_activities;i++){
    smart.Diary.selectActivity(stop_to_load.user_activities[i].stops_user_activity, stop_to_load.id, false);
  }

  //update main activity selector
  if(total_activities > 1){
    smart.Diary.showMainActivitySelector(stop_to_load.user_activities, stop_to_load.id);
  }
  else{
    smart.Diary.hideMainActivitySelector(stop_to_load.user_activities, stop_to_load.id);
  }
  //update answers

  smart.Diary.renderQuestions(stop_to_load.id, "activity");
}

smart.Diary.showMainActivitySelector = function(activity_list, stop_id){

  var mainSelectorRow = jQuery("#potencial_main-"+stop_id);
  var i, curr_image, user_act_id;

  jQuery(mainSelectorRow).html('');

  for(i=0;i<activity_list.length;i++){

    user_act_id = activity_list[i].stops_user_activity.user_activity_id;
    curr_image = smart.Diary._activitiesById[user_act_id].image;
    //check if element still doesnt exist in the selector
    if (!jQuery("#"+curr_image+"-"+stop_id+"-selected")[0]){
      //if not exists clone from activities
      var tmp_html = jQuery("#"+curr_image+"-"+stop_id).clone();

      //append selected to id
      var id = jQuery(tmp_html).attr('id');
      jQuery(tmp_html).attr('id', id+"-selected");
      //remove class selected
      jQuery(tmp_html).removeClass("sm_sprite-"+curr_image+"-selected");
      jQuery(tmp_html).addClass("sm_sprite-"+curr_image);
      //remove onClick method
      jQuery(tmp_html).removeAttr('onClick');
      jQuery(tmp_html).click({"stop_id": stop_id, "main_activity": user_act_id}, smart.Diary.updateMainActivity);
      //add to row
      jQuery(mainSelectorRow).append(tmp_html);
    }
  }

  smart.Diary.selectMainActivity(stop_id);

  jQuery("#hide_main_act-"+stop_id).show();
}

smart.Diary.selectMainActivity = function(stop_id){

  var stop_contents = smart.Diary.getStopRowFromStopId(stop_id);

  var main_act_id = smart.Diary._activitiesByImage[stop_contents["stop"].stop.main_activity].id;
  var activity = {};
  activity.user_activity_id = main_act_id
  smart.Diary.selectActivity(activity, stop_id, true);
}

smart.Diary.hideMainActivitySelector = function(activity_list, stop_id){
  var mainSelectorRow = jQuery("#potencial_main-"+stop_id);
  //remove every td from row
  jQuery(mainSelectorRow).html('');
  //hide selector
  jQuery("#hide_main_act-"+stop_id).hide();
}

smart.Diary.getModeKey = function(mode_id){
  // console.log("inside getModeKey for: " + mode_id);

  var mode_key;

  if (smart.Diary._modesById[mode_id]) {
    mode_key = smart.Diary._modesById[mode_id].image;
    if(mode_key.indexOf("/") >= 0) {
      mode_key = 'mode-default';
    }
  } else {
    mode_key = 'mode-default';
  }

  // console.log("returning -> " + mode_key);
  return mode_key;
}

smart.Diary.selectMode = function(mode_id, stop_id){
  //get selected mode for specific stop id
  //jQuery("[id^=mode-][id$=-"+stop_id+"][class$=selected]");
  var mode_key = smart.Diary.getModeKey(mode_id);
  if (jQuery("#"+mode_key+"-"+stop_id+".sm_sprite-"+mode_key+"-selected")[0]){
    return;
  }
  smart.Diary.toggleMode(mode_id, stop_id);
}

smart.Diary.selectActivity = function(activity, stop_id, main){
  var activity_image = smart.Diary._activitiesById[activity.user_activity_id].image;

  //check if the activity to be selected isn't already selected
  var id = main ? "#"+activity_image+"-"+stop_id+"-selected" : "#"+activity_image+"-"+stop_id;
  if (jQuery(id+".sm_sprite-"+activity_image+"-selected")[0]){
    return;
  }
  //toggle classes to select activity
  smart.Diary.toggleActivity(activity.user_activity_id, stop_id, main);
}

smart.Diary.toggleMode = function(mode_id, stop_id){
  var mode_key = smart.Diary.getModeKey(mode_id);
  jQuery("#"+mode_key+"-"+stop_id).toggleClass("sm_sprite-"+mode_key+"-selected");
  jQuery("#"+mode_key+"-"+stop_id).toggleClass("sm_sprite-"+mode_key);
}

smart.Diary.toggleActivity = function(activity_id, stop_id, main){

  var activity_image = smart.Diary._activitiesById[activity_id].image;

  var id = main ? "#"+activity_image+"-"+stop_id+"-selected" : "#"+activity_image+"-"+stop_id;

  jQuery(id).toggleClass("sm_sprite-"+activity_image+"-selected");
  jQuery(id).toggleClass("sm_sprite-"+activity_image);
}

smart.Diary.activityListUpdate = function(activity_id, stop_id){
  console.log(activity_id + " : " + stop_id)

  var activity_image = smart.Diary._activitiesById[activity_id].image;
  smart.Diary.toggleActivity(activity_id, stop_id, false);
  smart.Diary.updateStopActivities(stop_id);
}

smart.Diary.activitySelected = function(selector, stop_id) {
  // console.log(selector.val());
  if (selector.val()) {
    smart.Diary.activityListUpdate(selector.val(), stop_id);
  }
}

smart.Diary.updateStopActivities = function(stop_id){

  if(stop_id === undefined){
    console.log("NO STOP ID DEFINED.");
    return;
  }

  //get current selected activities
  var activities = jQuery("[id^=activity-][id$=-"+stop_id+"][class$=selected]");
  var i;
  var current_user_activities = [];

  for(i=0;i<activities.length;i++){
    var parts = activities[i].id.split("-");
    var image_name = parts[0]+"-"+parts[1];

    current_user_activities.push(smart.Diary._activitiesByImage[image_name].id);
  }

  jQuery.ajax({
    url: "/report/updateStopActivities",
    type: 'POST',
    data: { "stop_id": stop_id,
            "user_activities": current_user_activities
          },
    success: function(result) {
      var stop_contents = smart.Diary.getStopRowFromStopId(stop_id);
      stop_contents["stop"].stop.user_activities = result["user_activities"];
      stop_contents["stop"].stop.main_activity = result["main_activity"];
      smart.Diary.updateActivityContent(stop_contents["row"]);
      jQuery.publish("update-main-activity", {"stop_id": stop_id, "main_activity":stop_contents["stop"].stop.main_activity, "row": stop_contents["row"], "status": result["validated_flag"]});
      setStopValidatedStatus(stop_id, result["validated_flag"]);
    }
  });
}

smart.Diary.updateMainActivity = function(ev){

  var stop_id = ev.data.stop_id;
  var stop_contents = smart.Diary.getStopRowFromStopId(stop_id);

  var dataToUpdate = {}
  dataToUpdate["mainactivity"] = smart.Diary._activitiesById[ev.data.main_activity].image;
  var stopToValidate = smart.utils.buildValidatorObject(stop_contents["stop"].stop, dataToUpdate);
  var validatorResult = smart.ad_validator.run(["activity"], stopToValidate);

  var acceptMainActivity = function(data){
    if ("accept_activity" in data && data.accept_activity){
      smart.Diary.sendActivityToServer(ev.data.stop_id, ev.data.main_activity);
    } else {
      console.log('UPDATING MAIN ACTIVITY: user is going to change something');
    }
  }

  var overTimeActivity = function(step){
    smart.Alert.UserOverTime(stopToValidate, dataToUpdate["mainactivity"],
      function(){ //yes
        jQuery.publish(step._stepId,[{accept_activity: true}]);
      },
      function(){ //no
        jQuery.publish(step._stepId,[{accept_activity: false}]);
      });
  }
  var misplacedHomeActivity = function(step){
    //check if previous step already decided if it is supposed to
    //undo the selection
    if("accept_activity" in step._output && step._output.accept_activity==false){
      jQuery.publish(step._stepId,[step._output]);
      return;
    }

    smart.Alert.UserMisplacedHome(
      function(){//yes
        jQuery.publish(step._stepId,[{accept_activity: true}]);
      },
      function(){//no
        jQuery.publish(step._stepId,[{accept_activity: false}]);
      }
    );
  }

  var mainActivitySelector = new smart.Sequencer();
  mainActivitySelector.registerAfterHook(acceptMainActivity);
  if(validatorResult.length != 0){
    if (_.find(validatorResult, function(elm){ return elm.errorCode === 1020; })) { //not advisable duration
      mainActivitySelector.registerStep(new smart.SequenceStep(overTimeActivity, function(){} ));
    }
    if (_.find(validatorResult, function(elm){ return elm.errorCode === 1021; })) { // Different home
      mainActivitySelector.registerStep(new smart.SequenceStep(misplacedHomeActivity, function(){} ));
    }
    mainActivitySelector.run();
  }

  // console.log(validatorResult);
  if (validatorResult.length == 0){
    smart.Diary.sendActivityToServer(ev.data.stop_id, ev.data.main_activity);
  };
}

smart.Diary.sendActivityToServer = function(stop_id, main_activity){
  jQuery.ajax({
    url: "updateMainActivity",
    type: 'POST',
    data: { "stop_id": stop_id,
            "main_activity_id": main_activity
          },
    success: function(result) {
      stop_contents["stop"].stop.user_activities = result["user_activities"];
      stop_contents["stop"].stop.main_activity = result["new_main"]["image"];
      smart.Diary.updateActivityContent(stop_contents["row"]);
      jQuery.publish("update-main-activity", {"stop_id": stop_id, "main_activity":stop_contents["stop"].stop.main_activity, "row": stop_contents["row"], "status": result["validated_flag"]});
      setStopValidatedStatus(stop_id, result["validated_flag"]);
    }
  });
}


smart.Diary.clickedModeSelector = function(mode_id, stop_id){
  // console.log("mode_id -> " + mode_id);
  // console.log(smart.Diary._modesByImage);
  var curr_stop_info = smart.Diary.getStopRowFromStopId(stop_id);
  dataToUpdate = {};
  dataToUpdate["mode"] = smart.Diary.getModeKey(mode_id);
  var currentStop = smart.utils.buildValidatorObject(curr_stop_info["stop"].stop, dataToUpdate);
  var previousStop = null;

  if (curr_stop_info["row"]-1 >= 0){
    previousStop = smart.utils.buildValidatorObject(smart.Diary._currentDay['stops'][curr_stop_info["row"]-1].stop, {});
  }

  var validatorResult = smart.ad_validator.run(["mode"], currentStop, previousStop);

  if (validatorResult.length != 0){
    //prompt user for possible error
    smart.Alert.UserSpeeding(function(){
      console.log("user confirmed speeding");
      smart.Diary.sendModeToServer(stop_id, mode_id);
    },
    function(){
      console.log("user is going to rethink");
    });
  }
  else{
    smart.Diary.sendModeToServer(stop_id, mode_id);
  }

}

smart.Diary.sendModeToServer = function(stop_id, mode_id){

  var current_selected = jQuery("[id^=mode-][id$=-"+stop_id+"][class$=selected]");
  var id = current_selected.attr('id');
  if (id != null){
    var id_parts = id.split('-');
    current_selected.removeClass("sm_sprite-"+id_parts[0]+"-"+id_parts[1]+"-selected");
    current_selected.addClass("sm_sprite-"+id_parts[0]+"-"+id_parts[1]);
  }

  jQuery.ajax({
    url: "/report/updateTravelMode",
    type: 'POST',
    data: { "stop_id": stop_id,
            "mode_id": mode_id
          },
    success: function(result) {
      var stop_contents = smart.Diary.getStopRowFromStopId(stop_id);
      stop_contents["stop"].stop.mode_id = result["stop"].stop.mode_id;
      smart.Diary.updateTravelContent(stop_contents["row"]);

      jQuery.publish("update-travel-header", {"stop": stop_contents["stop"].stop, "row": stop_contents["row"]});
      setStopValidatedStatus(stop_id, result["validated_flag"]);
    }
  });
}

smart.Diary.updateRowHeader = function(ev, params){

  switch (ev.type){
    case "update-travel-header":
      var new_mode_image = smart.Diary._modesById[params["stop"].mode_id].image;
      var travel_i18nKey = "report.travel-modes." + new_mode_image;
      jQuery(".travel_"+params["stop"].id+" .sprite").attr("class", "sprite sprite-" + new_mode_image);
      var new_data_hint = I18n.t("report.travel_by_mode", {mode: I18n.t(travel_i18nKey, {defaultValue: travel_i18nKey}) });
      jQuery(".travel_"+params["stop"].id+" .sprite").closest('.hint--right').attr("data-hint", new_data_hint);
      break;

    case "update-main-activity":
      jQuery("#stop_number_" + params["stop_id"] + " .sprite").attr("class", "sprite sprite-" + params["main_activity"]);
      var i18nKey = "report.activities." + params["main_activity"];
      var new_title = I18n.t(i18nKey, {defaultValue: i18nKey});
      if (new_title == "Default") {
        new_title = I18n.t("report.no_info");
      };
      jQuery("#stop_number_"+params["stop_id"]).parent().attr("data-hint", new_title);
      // jQuery("#"+params["stop_id"]+" .stop_activity").html(new_title);
      break;

    case "update-time-headers":
      if (params["starttime_header"] != null){
        //update header starttime
        jQuery(".AccordionTitle.activity_"+params["stop_id"]+" .start_time_text .timevalue").html(params["starttime_header"]);
        //update previous travel segment endtime
        jQuery(".AccordionTitle.travel_"+params["stop_id"]+" .end_time_text .timevalue").html(params["starttime_header"]);
      }

      if (params["endtime_header"] != null){
        //update header endtime
        jQuery(".AccordionTitle.activity_"+params["stop_id"]+" .end_time_text .timevalue").html(params["endtime_header"]);
        //update next travel segment starttime
        jQuery(".AccordionTitle.activity_"+params["stop_id"]).parent().next().find(".AccordionTitle .start_time_text .timevalue").html(params["endtime_header"]);

      }

      if (params["prev_duration"] != null){
        jQuery(".AccordionTitle.travel_"+params["stop_id"]+" .duration").html(params["prev_duration"]);
      }
      if (params["next_duration"] != null){
        jQuery(".AccordionTitle.activity_"+params["stop_id"]).parent().next().find(".AccordionTitle .duration").html(params["next_duration"]);
      }
      jQuery(".AccordionTitle.activity_"+params["stop_id"]+" .duration").html(params["activity_duration"]);
      break;
  }
}

smart.Diary.showTimepickers = function(ev, row_id, stop_type, accord_id){
  // console.log("ui:open_row", row_id, stop_type, accord_id);
  var end_time_cell = jQuery('#'+accord_id + ' .end_time_cell');

  return; // don't show the end_time_cells for production yet

  if (stop_type === "activity") {
    // clone the end time picker from following travel row
    var activity_endtime_picker = jQuery('#end_time_'+row_id ).parent().clone(true, true);
    end_time_cell.empty().append(activity_endtime_picker.html());

  }
  else if(stop_type === "travel") {
    // clone the start time picker from following activity row, if available
    var stop_id = (""+row_id).match(/^Travel(.*)/)[1];
    var travel_endtime_picker = jQuery('#start_time_'+stop_id ).parent().clone(true, true);
    if (end_time_cell.length > 0) {
      end_time_cell.empty().append(travel_endtime_picker.html());
    }
  }
  jQuery('#'+accord_id + ' .end_time_cell select').attr('disabled', true);
}

smart.Diary.getStopRowFromStopId = function(stop_id){

  var row;
  var stop_contents = jQuery.grep(smart.Diary._currentDay['stops'], function(elem, index){
    if (elem.stop.id === stop_id)
      row = index;
    return elem.stop.id === stop_id;
  });

  return {"stop": stop_contents[0], "row": row};
}

smart.Diary.renderQuestions = function(stop_id, travel_activity){
  if(travel_activity == "travel"){
    smart.Diary.renderTravelQuestions(stop_id);
  }
  else if(travel_activity === "activity"){
    smart.Diary.renderActivityQuestions(stop_id);
  }
  else{
    console.log("missing parameter");
  }
}

smart.Diary.renderActivityQuestions=function(stop_id){
  var group_list = ["14"];
  jQuery.each(group_list, function(index, group){
    jQuery("#activitygroup_"+stop_id+"_"+group).hide();
  });

  stop_contents = smart.Diary.getStopRowFromStopId(stop_id);

  var other_activity = jQuery.grep(smart.Diary._currentDay['stops'][stop_contents["row"]].stop.user_activities, function(elem, index){
    return elem.stops_user_activity.user_activity_id == smart.Diary._activitiesByImage["activity-other"].id;
  });

  //if questions to be shown
  if (other_activity[0] || true){
    jQuery("#activitygroup_"+stop_id+"_14").show();
  }
  else{
    //clear other input value
    jQuery("#activitygroup_"+ stop_id +"_14 input").val('');
    jQuery("#activitygroup_"+stop_id+"_14").hide();
  }
}

smart.Diary.renderTravelQuestions=function(stop_id) {
  // var group_list = ["mode-car", "mode-bus", "mode-taxi", "mode-other", "mode-moto"];
  var group_list = ["mode-private_vehicle", "mode-car_club", "mode-bus", "mode-other",
    "mode-tube", "mode-taxi", "mode-national_rail", "mode-bike", "mode-bike_share"];

  jQuery.each(group_list, function(index, group){
    jQuery("#group_"+stop_id+"_"+group).hide();
  });

  stop_contents = smart.Diary.getStopRowFromStopId(stop_id);

  var mode = smart.Diary.getModeKey(stop_contents["stop"].stop.mode_id);
  // console.log("mode: " + mode);
  jQuery("#group_"+stop_id+"_"+mode).show();

  /*
    // NOTE: This has been commented as it's not needed any more! (post Nicola meeting)
    // Set activity-during-travel options based on the mode
    var all_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 14, 15, 16, 17]; // all base-lined options

    switch(mode) {
      case "mode-private_vehicle":
      case "mode-car_club":
      case "mode-bike":
      case "mode-bike_share":
        // Hide all options
        $("#group_" + stop_id + "_activity_during_travel_questions input[data-name='selectItemtravel_details[activity_during_travel]']").closest("li").hide();
        // $("#group_" + stop_id + "_activity_during_travel_questions input[data-name='selectItemtravel_details[activity_during_travel]']").prop("checked", false);

        var ids_to_remove = [1, 2, 4, 7];
        var ids_to_show = _.difference(all_ids, ids_to_remove);
        for (var index in ids_to_show) {
          $("#group_" + stop_id + "_activity_during_travel_questions input[data-name='selectItemtravel_details[activity_during_travel]'][value='" + ids_to_show[index] + "']").closest("li").show();
        }
        break;
      case "mode-bus":
        // Hide all options
        $("#group_" + stop_id + "_activity_during_travel_questions input[data-name='selectItemtravel_details[activity_during_travel]']").closest("li").hide();
        // $("#group_" + stop_id + "_activity_during_travel_questions input[data-name='selectItemtravel_details[activity_during_travel]']").prop("checked", false);

        var ids_to_add = [18];
        var ids_to_show = _.union(all_ids, ids_to_add);
        for (var index in ids_to_show) {
          $("#group_" + stop_id + "_activity_during_travel_questions input[data-name='selectItemtravel_details[activity_during_travel]'][value='" + ids_to_show[index] + "']").closest("li").show();
        }
        break;
      case "mode-tube":
      case "mode-national_rail":
        // Hide all options
        $("#group_" + stop_id + "_activity_during_travel_questions input[data-name='selectItemtravel_details[activity_during_travel]']").closest("li").hide();
        // $("#group_" + stop_id + "_activity_during_travel_questions input[data-name='selectItemtravel_details[activity_during_travel]']").prop("checked", false);

        var ids_to_add = [10, 12, 13, 19];
        var ids_to_show = _.union(all_ids, ids_to_add);
        for (var index in ids_to_show) {
          $("#group_" + stop_id + "_activity_during_travel_questions input[data-name='selectItemtravel_details[activity_during_travel]'][value='" + ids_to_show[index] + "']").closest("li").show();
        }
        break;
      default:
        // Hide all options
        $("#group_" + stop_id + "_activity_during_travel_questions input[data-name='selectItemtravel_details[activity_during_travel]']").closest("li").hide();
        // $("#group_" + stop_id + "_activity_during_travel_questions input[data-name='selectItemtravel_details[activity_during_travel]']").prop("checked", false);

        var ids_to_show = all_ids;
        for (var index in ids_to_show) {
          $("#group_" + stop_id + "_activity_during_travel_questions input[data-name='selectItemtravel_details[activity_during_travel]'][value='" + ids_to_show[index] + "']").closest("li").show();
        }
        break;
    }
  */

  var n_pax = jQuery("[id^=no_persons_"+stop_id+"]:checked").val();
  var n_pax_int = parseInt(n_pax, 10);

  var accompanying_suffix = "_ad-travel-household";

  if (n_pax_int != null && n_pax_int > 0){
    console.log("> 0");
    jQuery("#accompanying_"+stop_id+ accompanying_suffix).show();
  }
  else{
    console.log("0 or null");
    //make sure accompanying is deselected
    jQuery("#accompanying_"+stop_id+ accompanying_suffix).children().children().prop('checked', false);
    jQuery("#accompanying_"+stop_id+ accompanying_suffix).hide();

    // set it to 0
    jQuery("[id^=no_persons_"+stop_id+"][value='0']").prop("checked", true);
  }
}

smart.Diary.checkTimeBeforeChange = function(e, target, oldtime, newtime){


  var topElm = jQuery(target).parents('form');
  var curr_stop_id = whichAccordionIsOpen();

  var current_stop = previous_stop = next_stop = curr_stop_info=null;

  //console.log('checktime', topElm.data());

  var changedTime = "start_time";
  // if (jQuery(target).parent().attr('class').indexOf('end_time') > -1)
  var data = topElm.data();
  if (data.stopType === 'travel') {
    changedTime = "end_time";
  }

  var dataToUpdate = {};
  dataToUpdate[changedTime]= newtime;

  //get current stop
  var stop_row = smart.Diary._currentDay['stops'].length - 1;
  if (curr_stop_id > 0) {
    curr_stop_info = smart.Diary.getStopRowFromStopId(parseInt(curr_stop_id, 10));
    stop_row = curr_stop_info["row"];

    if (data.stopType === 'travel') {
      stop_row = curr_stop_info["row"] - 1;
    }
  }

  current_stop = smart.utils.buildValidatorObject(smart.Diary._currentDay['stops'][stop_row].stop, dataToUpdate);

  //get previous stop
  if (stop_row-1 >= 0){
    previous_stop = smart.utils.buildValidatorObject(smart.Diary._currentDay['stops'][stop_row-1].stop, {});
  }

  //get next stop
  if (curr_stop_id > 0 && stop_row+1 < smart.Diary._currentDay['stops'].length){
    next_stop = smart.utils.buildValidatorObject(smart.Diary._currentDay['stops'][stop_row+1].stop, {});
  }

  console.log("stopType", data.stopType, "prevStop", previous_stop, "currStop", current_stop, "nextStop", next_stop);
  //call validator
  var validatorResult = smart.ad_validator.run(["start_time", "end_time"], current_stop, previous_stop, next_stop);
  //check validator result
  console.log(validatorResult);

  var warningPlaceholder = jQuery(".timechange_warnings", topElm);
  var warningsTemplate = jQuery("#change_time_warnings");
  warningPlaceholder.html("");
  var isValid = true;
  for(var i=0;i<validatorResult.length;i++){
    console.log("validatorResult:", validatorResult[i]);
    warningPlaceholder.append(warningsTemplate.jqote({msg: validatorResult[i].message}));
    if (validatorResult[i].errorCode != 1020) isValid = false;
  }
  // store time validation status
  smart.Diary._currentDay['stops'][stop_row].stop["canSaveTime"] = isValid;

  if (current_stop.end_time < current_stop.start_time || isValid == false){
    console.log("endtime < startime");
    //jQuery('.change_time_button',topElm).hide();
    //jQuery('.invalid_button',topElm).show();
  }
}

smart.Diary.showHappinessQuestions = function(){
  // var chance = Math.random();
  // if (chance > 0.2) {
  //  jQuery('.happiness-questions').hide();
  // } else {
  //  jQuery('.happiness-questions').show();
  // }
  jQuery('.happiness-questions').show();
}

smart.Diary.getTravelModeDetails = function(mode_id) {
  var mode = smart.Diary._modesById[mode_id];
  if (mode == null){
    return {name: "Default", placeholder: I18n.t("report.no_info"), image: "mode-default"}
  } else {
    if (mode.name == "Default") {
      mode.placeholder = I18n.t("report.no_info");
      return mode;
    };
    var i18nKey = "report.travel-modes." + mode.image;
    mode.placeholder = I18n.t("report.travel_by_mode", {mode: I18n.t(i18nKey, {defaultValue: i18nKey}) });
    return mode;
  }
}

smart.Diary.showMetrics = function() {
  var distance_text = "Total distance covered: " +
    smart.Diary._currentDay['metrics']['total_distance'] + " km";

  var time_text = "Total travel time: " +
    smart.Diary._currentDay['metrics']['total_time'].hours + " hours " +
    smart.Diary._currentDay['metrics']['total_time'].minutes + " minutes";

  jQuery('#total_distance').text(distance_text);
  jQuery('#total_time').text(time_text);
}

smart.Diary.showMessages = function(result) {
  if (result.hasOwnProperty("message")) {
    jQuery('#flash_messages').html('<p class="' + result["message"]["class"] + '">' + result["message"]["text"] + '</p>');
  }
  else {
    jQuery('#flash_messages').html('');
  }
}

smart.Diary.clearMessages = function() {
  jQuery('#flash_messages').html('');
}

smart.Diary.init();
