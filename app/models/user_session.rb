class UserSession < Authlogic::Session::Base

	find_by_login_method :find_by_username_or_email

	def is_user_active?
		user = User.find_by_email(self.email)
		if (!user.nil?)
			user.active
		else
			false
		end
	end
end