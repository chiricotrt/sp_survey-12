# require 'delayed/recipes'

set_default(:delayed_job_pid_master) { "#{shared_path}/pids/master/delayed_job.pid" }
set_default(:delayed_job_pid_slave) { "#{shared_path}/pids/slave/delayed_job.pid" }
set_default(:delayed_job_workers_master, 6)
set_default(:delayed_job_workers_slave, 6)

# If you want to use command line options, for example to start multiple workers,
# define a Capistrano variable delayed_job_args:
#
#   set :delayed_job_args, "-n 2"

set_default(:delayed_job_args_master) {"--pid-dir=#{shared_path}/pids/master -p master --queue=general"} # Process prefix & queue
set_default(:delayed_job_args_slave) {"--pid-dir=#{shared_path}/pids/slave -p slave --queue=detectstops"}

namespace :delayed_job do
  def rails_env
    fetch(:rails_env, false) ? "RAILS_ENV=#{fetch(:rails_env)}" : ''
  end

  def delayed_job_command
    fetch(:delayed_job_command, 'script/delayed_job')
  end

  def roles
    [:master, :slave]
  end

  desc 'Stop the delayed_job process'
  task :stop, :roles => lambda { roles } do
  	parallel do |session|
  		session.when "in?(:slave)", "cd #{current_path};#{rails_env} #{delayed_job_command} stop #{delayed_job_args_slave}"
  		session.when "in?(:master)", "cd #{current_path};#{rails_env} #{delayed_job_command} stop #{delayed_job_args_master}"
  	end
  end

  desc 'Start the delayed_job process'
  task :start, :roles => lambda { roles } do
  	parallel do |session|
  		session.when "in?(:slave)", "cd #{current_path};#{rails_env} #{delayed_job_command} start -n #{delayed_job_workers_slave} #{delayed_job_args_slave}"
  		session.when "in?(:master)", "cd #{current_path};#{rails_env} #{delayed_job_command} start -n #{delayed_job_workers_master} #{delayed_job_args_master}"
  	end
  end

  desc 'Restart the delayed_job process'
  task :restart, :roles => lambda { roles } do
  	parallel do |session|
  		session.when "in?(:slave)", "cd #{current_path};#{rails_env} #{delayed_job_command} restart -n #{delayed_job_workers_slave} #{delayed_job_args_slave}"
  		session.when "in?(:master)", "cd #{current_path};#{rails_env} #{delayed_job_command} restart -n #{delayed_job_workers_master} #{delayed_job_args_master}"
  	end
  end
end

# Hook into Capistrano to start, stop and restart

after "deploy",    "delayed_job:stop"
#after "deploy:start",   "delayed_job:start"
#after "deploy:restart", "delayed_job:restart"