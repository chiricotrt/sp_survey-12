require "test_helper"

class NormalUserTest < Capybara::Rails::TestCase
  test "for adhoc testing" do
    skip('we are working on other tests for now')

    Capybara.current_driver = :selenium

    # load the necessary fixtures
    Fe.load_db 'seed_data'
    Fe.load_db 'surveys'
    Fe.load_db 'user_with_stops'

    # stubs
    ApplicationController.any_instance.stubs(:current_user).returns(User.first)
    User.any_instance.stubs(:completedPresurvey).returns(true)

    # test execution will pause here, allowing for live testing
    visit root_path
    binding.pry

    assert_content page, "Hello World"
    refute_content page, "Goobye All!"
  end
end
