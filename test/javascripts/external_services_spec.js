module("External Services");

test('Check Missing Params', 3, function() {
	var externalServices = new smart.ExternalServices();

	raises(function(){externalServices.runService(null);}, Error, "Must throw error on null first argument");
	raises(function(){externalServices.runService("onions");}, Error, "Must throw error on empty array as first argument");
	raises(function(){externalServices.runService(["foursquare_venues"]);}, Error, "Must throw error if missing parameters");

});