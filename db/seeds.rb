require 'exporter_service_logger'

# Create UCL test users
def create_ucl_test_users
  User.create(name: "Sridhar Raman", username: "sridhar", email: "sridhar.raman@gmail.com",
    password: "sridhar", password_confirmation: "sridhar",
    time_zone: Time.zone.name, active: true)

  # User.create(name: "Maria Kamargianni", username: "maria", email: "makam26@hotmail.com",
  #   password: "maria123", password_confirmation: "maria123",
  #   time_zone: Time.zone.name, active: true)
end

def populate_cities_manchester_luxembourg_budapest
  City.create(id: City::OXFORDSHIRE, name: "Oxfordshire")
  City.create(id: City::TURIN, name: "Turin")
  City.create(id: City::BUDAPEST, name: "Budapest")
end

def populate_cities_lat_lons
  city = City.where(id: City::OXFORDSHIRE).first
  city.update_attributes(lat: 53.4808, lon: -2.2426)

  city = City.where(id: City::TURIN).first
  city.update_attributes(lat: 49.6116, lon: 6.1319)

  city = City.where(id: City::BUDAPEST).first
  city.update_attributes(lat: 47.4979, lon: 19.0402)
end

def set_resident_user_type
  ActiveRecord::Base.connection.execute("update users set user_type = 'resident' where user_type is null")
end

def update_postal_codes_and_addresses
  EXPORTER_LOGGER.info("Running Seed.update_postal_codes_and_addresses")
  records = PreSurveyTravelPattern.where("home_location is not null and home_postcode is null")

  # Iterate over records and update home/work/shop/leisure_postcode+address
  records.each do |record|
    next if record.user.blank? or !record.user.resident_pre_survey_completed?
    EXPORTER_LOGGER.info("record -> #{record.id}")

    # Reverse geocode
    # HOME
    EXPORTER_LOGGER.info("HOME:")
    results = Geocoder.search("#{record.phome_lat},#{record.phome_lon}")
    record.home_postcode = results.blank? ? "NA" : results.first.postal_code
    record.home_address = results.blank? ? "NA" : results.first.address

    EXPORTER_LOGGER.info(record.home_postcode + "->" + record.home_address)

    # WORK
    if not record.work_locations.blank?
      EXPORTER_LOGGER.info("WORK:")
      lats = record.pwork_lat
      lons = record.pwork_lon

      if lats != Exporter::NULL_VALUE_NOT_ASKED
        work_postcodes = []
        work_addresses = []
        lats.each_with_index do |lat, index|
          results = Geocoder.search("#{lat},#{lons[index]}")
          results.blank? ? (work_postcodes << "NA") : (work_postcodes << results.first.postal_code)
          results.blank? ? (work_addresses << "NA") : (work_addresses << results.first.address)
        end

        record.work_postcodes = work_postcodes
        record.work_addresses = work_addresses

        EXPORTER_LOGGER.info(record.work_postcodes + "->" + record.work_addresses)
      end
    end

    # SHOPPING
    if not record.shop_locations.blank?
      EXPORTER_LOGGER.info("SHOP:")
      lats = record.pshop_lat
      lons = record.pshop_lon

      if lats != Exporter::NULL_VALUE_NOT_ASKED
        shop_postcodes = []
        shop_addresses = []
        lats.each_with_index do |lat, index|
          results = Geocoder.search("#{lat},#{lons[index]}")
          results.blank? ? (shop_postcodes << "NA") : (shop_postcodes << results.first.postal_code)
          results.blank? ? (shop_addresses << "NA") : (shop_addresses << results.first.address)
        end

        record.shop_postcodes = shop_postcodes
        record.shop_addresses = shop_addresses

        EXPORTER_LOGGER.info(record.shop_postcodes + "->" + record.shop_addresses)
      end
    end

    # LEISURE
    if not record.leisure_locations.blank?
      EXPORTER_LOGGER.info("LEISURE:")
      lats = record.pleis_lat
      lons = record.pleis_lon

      if lats != Exporter::NULL_VALUE_NOT_ASKED
        leisure_postcodes = []
        leisure_addresses = []
        lats.each_with_index do |lat, index|
          results = Geocoder.search("#{lat},#{lons[index]}")
          results.blank? ? (leisure_postcodes << "NA") : (leisure_postcodes << results.first.postal_code)
          results.blank? ? (leisure_addresses << "NA") : (leisure_addresses << results.first.address)
        end

        record.leisure_postcodes = leisure_postcodes
        record.leisure_addresses = leisure_addresses

        EXPORTER_LOGGER.info(record.leisure_postcodes + "->" + record.leisure_addresses)
      end
    end

    # Save record
    record.save!(validate: false)
  end
end

def update_home_postal_codes_and_addresses
  EXPORTER_LOGGER.info("Running Seed.update_home_postal_codes_and_addresses")
  records = PreSurveyTravelPattern.where("home_location is not null and home_postcode is null")

  # Iterate over records and update home_postcode+address
  records.each do |record|
    next if record.user.blank? or !record.user.resident_pre_survey_completed?
    EXPORTER_LOGGER.info("record -> #{record.id}")

    # Reverse geocode
    # HOME
    results = Geocoder.search("#{record.phome_lat},#{record.phome_lon}")
    record.home_postcode = results.blank? ? "NA" : results.first.postal_code
    record.home_address = results.blank? ? "NA" : results.first.address

    EXPORTER_LOGGER.info("#{record.home_postcode} -> #{record.home_address}")

    # Save record
    record.save!(validate: false)
  end
end

def update_work_postal_codes_and_addresses
  EXPORTER_LOGGER.info("Running Seed.update_work_postal_codes_and_addresses")
  records = PreSurveyTravelPattern.where("work_locations is not null and work_postcodes is null")

  # Iterate over records and update work_postcode+address
  records.each do |record|
    next if record.user.blank? or !record.user.resident_pre_survey_completed?
    EXPORTER_LOGGER.info("record -> #{record.id}")

    # Reverse geocode
    # WORK
    lats = record.pwork_lat
    lons = record.pwork_lon

    if lats != Exporter::NULL_VALUE_NOT_ASKED
      work_postcodes = []
      work_addresses = []
      lats.each_with_index do |lat, index|
        results = Geocoder.search("#{lat},#{lons[index]}")
        results.blank? ? (work_postcodes << "NA") : (work_postcodes << results.first.postal_code)
        results.blank? ? (work_addresses << "NA") : (work_addresses << results.first.address)
      end

      record.work_postcodes = work_postcodes
      record.work_addresses = work_addresses

      # EXPORTER_LOGGER.info(record.work_postcodes + "->" + record.work_addresses)
      EXPORTER_LOGGER.info("#{record.work_postcodes} -> #{record.work_addresses}")
    end

    # Save record
    record.save!(validate: false)
  end
end

def update_shopping_postal_codes_and_addresses
  EXPORTER_LOGGER.info("Running Seed.update_shopping_postal_codes_and_addresses")
  records = PreSurveyTravelPattern.where("shop_locations is not null and shop_postcodes is null")

  # Iterate over records and update shop_postcode+address
  records.each do |record|
    next if record.user.blank? or !record.user.resident_pre_survey_completed?
    EXPORTER_LOGGER.info("record -> #{record.id}")

    # Reverse geocode
    # SHOPPING
    lats = record.pshop_lat
    lons = record.pshop_lon

    if lats != Exporter::NULL_VALUE_MISSING_VALUE
      shop_postcodes = []
      shop_addresses = []
      lats.each_with_index do |lat, index|
        results = Geocoder.search("#{lat},#{lons[index]}")
        results.blank? ? (shop_postcodes << "NA") : (shop_postcodes << results.first.postal_code)
        results.blank? ? (shop_addresses << "NA") : (shop_addresses << results.first.address)
      end

      record.shop_postcodes = shop_postcodes
      record.shop_addresses = shop_addresses

      # EXPORTER_LOGGER.info(record.shop_postcodes + "->" + record.shop_addresses)
      EXPORTER_LOGGER.info("#{record.shop_postcodes} -> #{record.shop_addresses}")
    end

    # Save record
    record.save!(validate: false)
  end
end

def update_leisure_postal_codes_and_addresses
  EXPORTER_LOGGER.info("Running Seed.update_leisure_postal_codes_and_addresses")
  records = PreSurveyTravelPattern.where("leisure_locations is not null and leisure_postcodes is null")

  # Iterate over records and update home/work/shop/leisure_postcode+address
  records.each do |record|
    next if record.user.blank? or !record.user.resident_pre_survey_completed?
    EXPORTER_LOGGER.info("record -> #{record.id}")

    # LEISURE
    lats = record.pleis_lat
    lons = record.pleis_lon

    if lats != Exporter::NULL_VALUE_MISSING_VALUE
      leisure_postcodes = []
      leisure_addresses = []
      lats.each_with_index do |lat, index|
        results = Geocoder.search("#{lat},#{lons[index]}")
        results.blank? ? (leisure_postcodes << "NA") : (leisure_postcodes << results.first.postal_code)
        results.blank? ? (leisure_addresses << "NA") : (leisure_addresses << results.first.address)
      end

      record.leisure_postcodes = leisure_postcodes
      record.leisure_addresses = leisure_addresses

      # EXPORTER_LOGGER.info(record.leisure_postcodes + "->" + record.leisure_addresses)
      EXPORTER_LOGGER.info("#{record.leisure_postcodes} -> #{record.leisure_addresses}")
    end

    # Save record
    record.save!(validate: false)
  end

end

## MAIN CALL ##
# populate_cities_manchester_luxembourg_budapest
# populate_cities_lat_lons

# set_resident_user_type
# update_postal_codes_and_addresses
update_home_postal_codes_and_addresses
# update_work_postal_codes_and_addresses
# update_shopping_postal_codes_and_addresses
# update_leisure_postal_codes_and_addresses