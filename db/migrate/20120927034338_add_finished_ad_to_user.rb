class AddFinishedAdToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :finished_ad, :timestamp
  end

  def self.down
    remove_column :users, :finished_ad
  end
end
