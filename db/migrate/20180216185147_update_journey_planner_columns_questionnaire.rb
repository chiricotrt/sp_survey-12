class UpdateJourneyPlannerColumnsQuestionnaire < ActiveRecord::Migration
  def change
    remove_column :pre_survey_journey_planner_characteristics, :other_journey_planner_name, :string

    add_column :pre_survey_journey_planner_characteristics, :decision_status, :integer
    add_column :pre_survey_journey_planner_characteristics, :journey_planner_usage, :string

    add_column :pre_survey_journey_planner_characteristics, :journey_planner_trip_1, :integer
    add_column :pre_survey_journey_planner_characteristics, :journey_planner_trip_2, :integer
    add_column :pre_survey_journey_planner_characteristics, :journey_planner_trip_3, :integer
    add_column :pre_survey_journey_planner_characteristics, :journey_planner_trip_4, :integer
    add_column :pre_survey_journey_planner_characteristics, :journey_planner_trip_5, :integer
    add_column :pre_survey_journey_planner_characteristics, :journey_planner_trip_6, :integer
    add_column :pre_survey_journey_planner_characteristics, :journey_planner_trip_7, :integer

    add_column :pre_survey_journey_planner_characteristics, :attitude_towards_journey_planner_1, :integer
    add_column :pre_survey_journey_planner_characteristics, :attitude_towards_journey_planner_2, :integer
    add_column :pre_survey_journey_planner_characteristics, :attitude_towards_journey_planner_3, :integer
  end
end
