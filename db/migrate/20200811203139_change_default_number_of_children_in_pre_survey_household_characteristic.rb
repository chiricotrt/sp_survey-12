class ChangeDefaultNumberOfChildrenInPreSurveyHouseholdCharacteristic < ActiveRecord::Migration
  def change
    change_column :pre_survey_household_characteristics, :number_of_children_5, :integer, default: 0
    change_column :pre_survey_household_characteristics, :number_of_children_6_12, :integer, default: 0
    change_column :pre_survey_household_characteristics, :number_of_children_13_17, :integer, default: 0
  end
end
