class AddCompleteToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :complete, :boolean, :default => 0
  end

  def self.down
    remove_column :users, :complete
  end
end
