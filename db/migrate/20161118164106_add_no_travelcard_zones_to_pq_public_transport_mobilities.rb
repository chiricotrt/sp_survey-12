class AddNoTravelcardZonesToPqPublicTransportMobilities < ActiveRecord::Migration
  def change
    add_column :pq_public_transport_mobilities, :no_travelcard_zones, :string
  end
end
