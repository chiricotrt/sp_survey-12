class AddSingleAccessTokenToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :single_access_token, :string, null: false

 		# Generate a new single access token for each existing user
  	User.all.map{ |u| u.reset_single_access_token!}
  end
end