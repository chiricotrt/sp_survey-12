class AddVariableDataToSpPlan < ActiveRecord::Migration
  def change
    add_column :sp_plans, :variable_data, :text
  end
end
