class AddPriceMultiplierToGeneratedPlans < ActiveRecord::Migration
  def change
    add_column :generated_plans, :price_multiplier, :float
  end
end
