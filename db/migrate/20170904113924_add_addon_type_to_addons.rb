class AddAddonTypeToAddons < ActiveRecord::Migration
  def change
    add_column :addons, :addon_type, :string
  end
end
